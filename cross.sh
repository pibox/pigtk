#!/bin/bash -p

# Allow setting toolchain, staging and opkg directory from command line.
while getopts ":o:s:t:" Option
do
    case $Option in
    t) TC=$OPTARG;;
    s) STAGING=$OPTARG;;
    o) OPKG_DIR=$OPTARG;;
    *) echo "./cross.sh [ -o dir | -s dir | -t dir ]"; exit 0;;
    esac
done

# Note: STAGING_DIR is the <buildroot>/output/staging generated from the "make buildroot" for PiBox.
if [ "$STAGING" != "" ] 
then
	export STAGING_DIR=$STAGING
else
	if [ "$BR" != "" ] 
	then
		export STAGING_DIR=$BR/output/staging
	else
		echo "You must set BR to the top of your buildroot tree or STAGING to where the staging tree lives."
		exit 1
	fi
fi

# Toolchain should be the toolchain built with PiBox and installed as an RPM.
if [ "$TC" != "" ] 
then
	export TOOLCHAIN=$TC
else
	echo "You must set TC to where the cross toolchain lives."
	exit 1
fi

if [ -f config.log ]
then
	make distclean
fi

if [ -f /etc/centos-release ]
then
	LDFLAGSVAL="--sysroot=$STAGING_DIR"
else
	LDFLAGSVAL="--sysroot=$STAGING_DIR -L$STAGING_DIR/usr/lib -L$STAGING_DIR/lib"
fi

# Get the toolchain prefix for the compiler tools
TCPREFIX=$(cd ${TC}/bin && ls -1 *gcc | sed 's/-gcc//')

echo "Toolchain   : $TOOLCHAIN"
echo "Staging tree: $STAGING_DIR"
echo "OPKG directory  : $OPKG_DIR"
echo "Toolchain prefix: $TCPREFIX"

autoreconf -i

PKG_CONFIG_SYSROOT_DIR=$STAGING_DIR \
    PATH=$TOOLCHAIN/bin:$PATH \
    CFLAGS="--sysroot=$STAGING_DIR -I$STAGING_DIR/usr/lib/glib-2.0/include -I$STAGING/usr/lib/gtk-2.0/include" \
    LDFLAGS="$LDFLAGSVAL" \
    ./configure --host="${TCPREFIX}" --prefix=/usr

PATH=$TOOLCHAIN/bin:$PATH \
	CFLAGS="--sysroot=$STAGING_DIR" \
	LDFLAGS="$LDFLAGSVAL" \
	make

PATH=$TOOLCHAIN/bin:$PATH \
	OPKG_DIR=$OPKG_DIR DESTDIR=`pwd`/install \
	make install pkg

