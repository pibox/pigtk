/*******************************************************************************
 * pigtk widget library
 *
 * pigtk-icontable.c:  Icontable widget (aka gtk-icontable)
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 * 
 * Based on:
 * Custom GTK+ widget - http://zetcode.com/tutorials/gtktutorial/customwidget/
 * Clock Widget - https://github.com/humbhenri/clocks/tree/master/gtk-clock
 ******************************************************************************/
#define PIGTK_ICONTABLE_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <glib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include <pibox/log.h>
#include <pibox/parson.h>
#include <pibox/pibox.h>
#include "pigtk-icontable.h"

static char *icontableFile;

#define FONTSIZE 10

enum {
  ICONTABLE_SIGNAL,
  LAST_SIGNAL
};

static gint icontable_signals[LAST_SIGNAL] = { 0 };
static guint new_rows, new_cols;

/*
 *========================================================================
 * Prototypes
 *========================================================================
 */
static void gtk_icontable_class_init(GtkIcontableClass *klass);
static void gtk_icontable_init(GtkIcontable *icontable);
static void gtk_icontable_size_request(GtkWidget *widget, GtkRequisition *requisition);
static void gtk_icontable_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void gtk_icontable_realize(GtkWidget *widget);
static gboolean gtk_icontable_expose(GtkWidget *widget, GdkEventExpose *event);
static void gtk_icontable_paint(GtkWidget *widget);
static void gtk_icontable_destroy(GtkObject *object);

/*
 *========================================================================
 *========================================================================
 * Private Functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   getCell
 * Prototype:  void getCell( GtkIcontable *, int )
 *
 * Description:
 * Retrieve the configuration for the icon at the specified index.
 * 
 * Arguments:
 * GtkIcontable *itable The main widget structure.
 * int index            The index (row, col) to the cell configuration to retrieve.
 *
 * Returns:
 * The GSList entry at the specified index or NULL if the index doesn't exist.
 *========================================================================
 */
static GSList *
getCell(GtkIcontable *itable, int idx)
{
    piboxLogger(LOG_TRACE1, "Retrieving cell %d of %d.\n", idx, g_slist_length(itable->icons));
    return ( g_slist_nth(itable->icons, idx) );
}

/*
 *========================================================================
 * Name:   do_drawing
 * Prototype:  void do_drawing( GtkWidget *, int, int, int )
 *
 * Description:
 * Updates a specific cell of the pixmap in the icontable widget.
 * 
 * Arguments:
 * GtkWidget *      The widget in which we'll do the drawing           
 * int              Row of the button to update.
 * int              Column of the button to update.
 * int              0: no highlight; 1: draw a highlight beneath the image; 2: clear highlight
 *========================================================================
 */
static void 
do_drawing( GtkIcontable *itable, int row, int col, int doHighlight)
{
    GdkPixmap       *pixmap;
    cairo_surface_t *cst;
    cairo_t         *cr = NULL;
    cairo_t         *cr_pixmap;
    gdouble         offset_x, offset_y;
    gint            width, height;
    gint            realCellWidth;
    gint            realCellHeight;
    gint            iconWidth;
    gint            iconHeight;
    guint           drawOutline;
    GdkPixbuf       *image = NULL;
    GdkPixbuf       *newimage;
    GSList          *cell;
    GtkIconCell     *entry;
    const char      *icon;
    const char      *color;
    char            *filename;

    cairo_text_extents_t    extents;
    struct stat             stat_buf;
    PangoFontDescription    *desc;
    PangoRectangle          pangoRectangle;
    PangoAttrList           *attrs = NULL;
    PangoLayout             *layout;
    char                    buf[256];

    if ( itable->pixmap == NULL )
    {
        piboxLogger(LOG_TRACE1, "No pixmap to draw in.\n");
        return;
    }
    pixmap = itable->pixmap;

    // Compute icon position relative to itable->pixmap;
    offset_x = itable->cellWidth * col;
    offset_y = itable->cellHeight * row;

    // create a gtk-independant surface to draw on
    if ( col == (itable->cols - 1) && itable->col_pad > 0 )
    {
        // Last column can be padded up to a full cell width - 1.
        realCellWidth = itable->cellWidth+itable->col_pad;
    }
    else
    {
        realCellWidth = itable->cellWidth;
    }
    if ( row == (itable->rows - 1) && itable->row_pad > 0 )
    {
        // Last row can be padded up to a full cell height - 1.
        realCellHeight = itable->cellHeight+itable->row_pad;
    }
    else
    {
        realCellHeight = itable->cellHeight;
    }
    piboxLogger(LOG_TRACE1, "CellWidth: %d, CellHeight: %d\n", realCellWidth, realCellHeight);
    cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, realCellWidth, realCellHeight);
    cr = cairo_create(cst);

    /* Set to default background */
    piboxLogger(LOG_TRACE1, "Setting cell background to default colors.\n");
    cairo_set_source_rgba(cr, itable->bg.red/65535, itable->bg.green/65535, itable->bg.blue/65535, 1.0);
    cairo_rectangle(cr, 0, 0, (double)realCellWidth, (double)realCellHeight);
    cairo_fill(cr);
    cairo_paint(cr);

    /* Retrieve configuration for this cell. */
    piboxLogger(LOG_TRACE1, "Index + row*cols + col : %d + %d*%d + %d\n", itable->index, row, itable->cols, col);
    cell = getCell(itable, (itable->page * (itable->rows*itable->cols)) + (row*itable->cols)+col);
    if ( cell == NULL )
    {
        piboxLogger(LOG_TRACE1, "No such cell at index %d\n", (int)(row*itable->cols)+col);
        goto do_drawing_exit;
    }
    entry = (GtkIconCell *)g_slist_nth_data(cell, 0);
    if ( entry == NULL )
    {
        piboxLogger(LOG_TRACE1, "No data in cell at index %d\n", (int)(row*itable->cols)+col);
        goto do_drawing_exit;
    }

    /*
     * If requested, draw an outline as a "highlight"
     */
    if ( doHighlight == 0 )
    {
        if ( entry->highlight == NULL )
        {
            piboxLogger(LOG_TRACE1, "Drawing normal outline.\n");
            cairo_set_source_rgba(cr, 0, 0, 0, 1.0);
            cairo_move_to(cr, 0, 0);
            cairo_line_to(cr, (double)realCellWidth, 0);
            cairo_line_to(cr, (double)realCellWidth, (double)realCellHeight);
            cairo_line_to(cr, 0, (double)realCellHeight);
            cairo_line_to(cr, 0, 0);
            cairo_set_line_width(cr, 2);
            cairo_stroke(cr); 
        }
    }
    else
    {
        drawOutline = 1;
        if ( entry->highlight != NULL )
        {
            /* Use the user-provided highlight image. */
            filename = (char *)calloc(1, strlen(itable->icontableDir) + strlen(entry->highlight) + 2);
            sprintf(filename, "%s/%s", itable->icontableDir, entry->highlight);
            if (stat(filename, &stat_buf) == 0)
            {
                /* Isolate transformations - save our current context. */
                cairo_save(cr);

                /* Set the highlight size */
                iconWidth = realCellWidth - itable->cell_pad;
                iconHeight = realCellHeight - itable->cell_pad;
                piboxLogger(LOG_TRACE1, "IconWidth: %d, IconHeight: %d\n", itable->cell_pad, iconWidth, iconHeight);

                /* Draw the highlight image. */
                piboxLogger(LOG_TRACE1, "Setting highlight to icon: %s\n", filename);
                image = gdk_pixbuf_new_from_file(filename, NULL);
                width  = gdk_pixbuf_get_width(image);
                height = gdk_pixbuf_get_height(image);
                piboxLogger(LOG_TRACE1, "Icon image: w/h: %d / %d \n", width, height);
                newimage = gdk_pixbuf_scale_simple(image, iconWidth, iconHeight, GDK_INTERP_BILINEAR);
                cairo_translate(cr, (itable->cell_pad / 2), (itable->cell_pad/2));
                g_object_unref(image);
                image = newimage;

                /* Positioning */
                gdk_cairo_set_source_pixbuf(cr, image, 0, 0);
                cairo_paint(cr);    
                g_object_unref(image);

                /* Restore original context. */
                cairo_restore(cr);

                /* Disable default outline drawing. */
                drawOutline = 0;

            }
            free(filename);
        }

        if ( drawOutline )
        {
            piboxLogger(LOG_TRACE1, "Drawing hightlight outline.\n");
            cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.5);
            cairo_move_to(cr, 0, 0);
            cairo_line_to(cr, (double)realCellWidth, 0);
            cairo_line_to(cr, (double)realCellWidth, (double)realCellHeight);
            cairo_line_to(cr, 0, (double)realCellHeight);
            cairo_line_to(cr, 0, 0);
            cairo_set_line_width(cr, 10);
            cairo_stroke(cr); 
        }
    }

    /* Set the icon size */
    iconWidth = realCellWidth - itable->cell_pad;
    iconHeight = realCellHeight - itable->cell_pad;
    piboxLogger(LOG_TRACE1, "Cell_pad: %d, IconWidth: %d, IconHeight: %d\n", itable->cell_pad, iconWidth, iconHeight);

    /* Load the cell icon. */
    filename = (char *)calloc(1, strlen(itable->icontableDir) + strlen(entry->icon) + 2);
    sprintf(filename, "%s/%s", itable->icontableDir, entry->icon);
    piboxLogger(LOG_TRACE1, "Setting cell to icon: %s\n", filename);
    image = gdk_pixbuf_new_from_file(filename, NULL);
    width  = gdk_pixbuf_get_width(image);
    height = gdk_pixbuf_get_height(image);
    piboxLogger(LOG_TRACE1, "Icon image: w/h: %d / %d \n", width, height);
    newimage = gdk_pixbuf_scale_simple(image, iconWidth, iconHeight, GDK_INTERP_BILINEAR);
    cairo_translate(cr, (itable->cell_pad / 2), (itable->cell_pad/2));
    g_object_unref(image);
    free(filename);
    image = newimage;

    /* Positioning */
    gdk_cairo_set_source_pixbuf(cr, image, 0, 0);
    cairo_paint(cr);    
    g_object_unref(image);

    if ( (entry->label != NULL) && itable->showLabel )
    {
        piboxLogger(LOG_TRACE1, "Text to display: %s\n", entry->label);
        layout = pango_cairo_create_layout(cr);
        sprintf(buf, "Nunito Bold %d", FONTSIZE);
        desc = pango_font_description_from_string( buf );
        pango_font_description_set_absolute_size(desc, FONTSIZE*PANGO_SCALE);
        pango_layout_set_font_description(layout, desc);
        pango_font_description_free(desc);
        pango_layout_set_width(layout, -1); // Disable wrap
        pango_layout_set_spacing(layout, 1);
        pango_layout_set_single_paragraph_mode(layout, FALSE);
        pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);
    
        /* Initialize attributes. */
        attrs = pango_attr_list_new();
        pango_attr_list_insert (attrs, pango_attr_underline_new(PANGO_UNDERLINE_NONE));
        pango_layout_set_attributes (layout, attrs);
    
        pango_layout_set_text(layout, entry->label, -1);
    
        pango_layout_get_pixel_size(layout, &pangoRectangle.width, &pangoRectangle.height );
        piboxLogger(LOG_TRACE4, "text w/h: %d / %d\n", pangoRectangle.width, pangoRectangle.height);
        cairo_set_source_rgba(cr, itable->labelColor.red/65535, itable->labelColor.green/65535, itable->labelColor.blue/65535, 1.0);
        // cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
        pango_cairo_update_layout(cr, layout);
    
        /* Don't need to center horizontally because pango_layout_set_alignment does that for us. */
        // piboxLogger(LOG_TRACE1, "**** HO: rowHeight: %d\n", rowHeight);
        // piboxLogger(LOG_TRACE1, "**** HO: pangoRectH: %d\n", pangoRectangle.height);
        // piboxLogger(LOG_TRACE1, "**** HO: Horizontal offset: %d\n", (rowHeight-pangoRectangle.height)/2);
        cairo_translate(cr, (realCellWidth-pangoRectangle.width)/2, realCellHeight-pangoRectangle.height);
        pango_cairo_show_layout(cr, layout);
    
        /* Cleanup pango */
        pango_attr_list_unref(attrs);
        g_object_unref(layout);
    }
    else
        piboxLogger(LOG_TRACE1, "No text to display.\n");


do_drawing_exit:
    cairo_destroy(cr);
    cr = NULL;

    /* Transfer this drawing to our widget pixmap. */
    cr_pixmap = gdk_cairo_create(pixmap);
    cairo_set_source_surface (cr_pixmap, cst, offset_x, offset_y);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);
    cairo_surface_destroy(cst);
}

/*
 *========================================================================
 *========================================================================
 * Private API
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:    gtk_icontable_get_type
 * Prototype:   void gtk_icontable_get_type( void )
 *
 * Description:
 * Returns the GtkType for this widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
GtkType
gtk_icontable_get_type(void)
{
    static GtkType gtk_icontable_type = 0;
    if (!gtk_icontable_type) {
        static const GtkTypeInfo gtk_icontable_info = {
            "GtkIcontable",
            sizeof(GtkIcontable),
            sizeof(GtkIcontableClass),
            (GtkClassInitFunc) gtk_icontable_class_init,
            (GtkObjectInitFunc) gtk_icontable_init,
            NULL,
            NULL,
            (GtkClassInitFunc) NULL
        };
        gtk_icontable_type = gtk_type_unique(GTK_TYPE_WIDGET, &gtk_icontable_info);
    }
    return gtk_icontable_type;
}

/*
 *========================================================================
 * Name:    gtk_icontable_new
 * Prototype:   void gtk_icontable_new( int, int )
 *
 * Description:
 * Create the gtk_icontable instance.  Sets the layout in rows and columns.
 *
 * Arguments:
 * int rows     Number of rows to create.
 * int cols     Number of columns to create.
 *========================================================================
 */
GtkWidget *gtk_icontable_new(guint rows, guint cols)
{
    new_rows = rows;
    new_cols = cols;
    return GTK_WIDGET( gtk_type_new(gtk_icontable_get_type()) );
}

/*
 *========================================================================
 * Name:   gtk_icontable_class_init
 * Prototype:  void gtk_icontable_class_init( GtkIcontableClass * )
 *
 * Description:
 * Initialize the widget class structure fields and setup signals.
 * 
 * Arguments:
 * GtkIcontableClass *      The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_icontable_class_init(GtkIcontableClass *klass)
{
    GtkWidgetClass *widget_class;
    GtkObjectClass *object_class;

    widget_class = (GtkWidgetClass *) klass;
    object_class = (GtkObjectClass *) klass;

    widget_class->realize       = gtk_icontable_realize;
    widget_class->size_request  = gtk_icontable_size_request;
    widget_class->size_allocate = gtk_icontable_size_allocate;
    widget_class->expose_event  = gtk_icontable_expose;
    object_class->destroy       = gtk_icontable_destroy;
}

/*
 *========================================================================
 * Name:   gtk_icontable_init
 * Prototype:  void gtk_icontable_init( GtkIcontable * )
 *
 * Description:
 * Initialize the widget object and create composite window components.
 * The icontable widget is not a composite, however.  It's just a giant
 * pixmap we draw in.
 * 
 * Arguments:
 * GtkIcontable *      The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_icontable_init(GtkIcontable *itable)
{
    int             numRows, numCols;
    int             row, col;
    GtkWidget       *dwga;
    GtkWidget       *button;
    GtkObjectClass  *object_class;
    int             idx;
    char            *buf;
    struct stat     stat_buf;

    itable->icons = NULL;
    itable->index = 0;
    itable->list_length = 0;
    itable->rows = new_rows;
    itable->cols = new_cols;
    itable->cell_pad = 0;
    itable->showLabel = TRUE;

    if ( getenv("GTK_ICONTABLE_DIR") != NULL )
        itable->icontableDir = strdup( getenv("GTK_ICONTABLE_DIR") );
    else
        itable->icontableDir = strdup(GTK_ICONTABLE_DIR);

    object_class = (GtkObjectClass *)itable;
}

/*
 *========================================================================
 * Name:   gtk_icontable_size_request
 * Prototype:  void gtk_icontable_size_request( GtkWidget *, GtkRequisition * )
 *
 * Description:
 * Tell GTK+ what size the widget should be, initially.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkRequisition *     The widgets requested size to pass back to caller.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_icontable_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
    GdkScreen       *screen;
    gint            monitor;
    GdkRectangle    dest;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_ICONTABLE(widget));
    g_return_if_fail(requisition != NULL);
}

/*
 *========================================================================
 * Name:   gtk_icontable_size_allocate
 * Prototype:  void gtk_icontable_size_allocate( GtkWidget *, GtkAllocation * )
 *
 * Description:
 * Handle widget resizes.  Basically this means remove the reference to the
 * pixmap (which should free it), reset the size for the widget, then
 * reallocate and paint in the pixmap at the new size.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkAllocation *      The size allocated for the widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_icontable_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
    GtkIcontable *itable;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_ICONTABLE(widget));
    g_return_if_fail(allocation != NULL);

    widget->allocation = *allocation;
    piboxLogger(LOG_TRACE1, "Allocated w,h: %d,%d\n", allocation->width, allocation->height);
    piboxLogger(LOG_TRACE1, "Allocated x,y: %d,%d\n", allocation->x, allocation->y);
    itable = GTK_ICONTABLE(widget);
    itable->width = allocation->width;
    itable->height = allocation->height;
    itable->cellWidth = allocation->width / itable->cols;
    itable->cellHeight = allocation->height / itable->rows;
    itable->col_pad = allocation->width % itable->cols;
    itable->row_pad = allocation->height % itable->rows;
    piboxLogger(LOG_TRACE1, "Cell x,y,cpad,rpad: %d,%d,%d,%d\n", 
            itable->cellWidth, itable->cellHeight, itable->col_pad, itable->row_pad);
    if ( itable->pixmap != NULL )
    {
        g_object_unref(itable->pixmap);
        itable->pixmap = NULL;
    }
    if (GTK_WIDGET_REALIZED(widget)) {
        gdk_window_move_resize(
            widget->window,
            allocation->x, allocation->y,
            allocation->width, allocation->height
        );
    }
    gtk_icontable_paint(widget);
}

/*
 *========================================================================
 * Name:   gtk_icontable_realize
 * Prototype:  void gtk_icontable_realize( GtkWidget * )
 *
 * Description:
 * After creating the window, we set its style and background, and put a
 * pointer to the widget in the user data field of the GdkWindow. This
 * last step allows GTK to dispatch events for this window to the correct
 * widget. 
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_icontable_realize(GtkWidget *widget)
{
    GtkIcontable *itable;
    GdkWindowAttr attributes;
    guint attributes_mask;
    GtkStyle *style;
    GtkAllocation alloc;
    int row, col;
    int idx;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_ICONTABLE(widget));

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    gtk_widget_get_allocation(widget, &alloc);

    piboxLogger(LOG_TRACE1, "alloc.x: %d\n", alloc.x);
    piboxLogger(LOG_TRACE1, "alloc.y: %d\n", alloc.y);
    piboxLogger(LOG_TRACE1, "alloc.w: %d\n", alloc.width);
    piboxLogger(LOG_TRACE1, "alloc.h: %d\n", alloc.height);

    itable = GTK_ICONTABLE(widget);
    itable->width = alloc.width;
    itable->height = alloc.height;
    itable->cellWidth = alloc.width / itable->cols;
    itable->cellHeight = alloc.height / itable->rows;
    piboxLogger(LOG_TRACE1, "cellWidth: %d\n", itable->cellWidth);
    piboxLogger(LOG_TRACE1, "cellHeight: %d\n", itable->cellHeight);

    itable->labelColor.red = 65535;
    itable->labelColor.green = 65535;
    itable->labelColor.blue = 65535;

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x           = alloc.x;
    attributes.y           = alloc.y;
    attributes.width       = alloc.width;
    attributes.height      = alloc.height;
    attributes.wclass      = GDK_INPUT_OUTPUT;
    attributes.event_mask  = gtk_widget_get_events(widget) | GDK_EXPOSURE_MASK;
    attributes_mask        = GDK_WA_X | GDK_WA_Y;

    widget->window = gdk_window_new(
        gtk_widget_get_parent_window (widget),
        & attributes, attributes_mask
    );

    style = gtk_widget_get_style (widget);
    if (style != NULL) 
    {
        GTK_ICONTABLE(widget)->bg = style->bg[GTK_STATE_NORMAL];
        GTK_ICONTABLE(widget)->fg = style->fg[GTK_STATE_NORMAL];
    }
    gtk_icontable_paint(widget);

    gdk_window_set_user_data(widget->window, widget);
    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
}

/*
 *========================================================================
 * Name:   gtk_icontable_expose
 * Prototype:  void gtk_icontable_expose( GtkWidget *, GdkEventExpose * )
 *
 * Description:
 * All drawing is done in an off-screen pixmap.  This function copies
 * that pixmap to the widget window to make the updates visible.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkRequisition *     The widgets requested size to pass back to caller.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static gboolean
gtk_icontable_expose(GtkWidget *widget, GdkEventExpose *event)
{
    int width, height;

    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_ICONTABLE(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);

    // Copy the entire pixmap over the widget's drawing area.
    if ( GTK_ICONTABLE(widget)->pixmap != NULL )
    {
        piboxLogger(LOG_TRACE1, "widget->w/h: %d/%d\n", widget->allocation.width, widget->allocation.height);
        gdk_drawable_get_size(GTK_ICONTABLE(widget)->pixmap, &width, &height);
        piboxLogger(LOG_TRACE1, "pixmap w/h: %d/%d\n", width, height);

        gdk_draw_drawable(widget->window,
            widget->style->fg_gc[GTK_WIDGET_STATE(widget)], GTK_ICONTABLE(widget)->pixmap,
            event->area.x, event->area.y,
            event->area.x, event->area.y,
            event->area.width, event->area.height);
    }
    return TRUE;
}

/*
 *========================================================================
 * Name:    gtk_icontable_paint
 * Prototype:   void gtk_icontable_paint( GtkWidget *widget )
 *
 * Description:
 * Handle painting (data update) of the widget.
 * This is the front end to the do_drawing() function that iterates the
 * complete set of cells in the icontable.
 *
 * Arguments:
 * GtkWidget *widget        The widget to paint.
 *
 * Notes:
 * This is NOT a standard GTK widget API function.
 *========================================================================
 */
static void
gtk_icontable_paint(GtkWidget *widget)
{
    GtkIcontable             *itable;
    int                     i,j;
    static int              painting = 0;

    /* Avoid multiple calls */
    if ( painting )
        return;
    painting = 1;

    itable = GTK_ICONTABLE( widget );
    if ( GTK_ICONTABLE(widget)->pixmap == NULL )
    {   
        piboxLogger(LOG_TRACE1, "Allocating pixmap\n");
        itable->pixmap =
            gdk_pixmap_new(widget->window,widget->allocation.width,widget->allocation.height,-1);
    }
    if ( itable->pixmap == NULL )
    {
        piboxLogger(LOG_TRACE1, "pixmap allocation failed.\n");
        painting = 0;
        return;
    }

    /*
     * Draw each icon, one at a time.
     */
    for (i=0; i<itable->rows; i++)
    {
        for (j=0; j<itable->cols; j++)
        {
            do_drawing(itable, i, j, 0);
        }
    }
    painting = 0;
}

/*
 *========================================================================
 * Name:   gtk_icontable_destroy
 * Prototype:  void gtk_icontable_destroy( GtkObject * )
 *
 * Description:
 * Clean up the widget when it is removed.  This is only called,
 * at least for this widget, when the application window is closed.
 * 
 * Arguments:
 * GtkObject *          The object class of the widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_icontable_destroy(GtkObject *object)
{
    GtkIcontable *itable;
    GtkIcontableClass *klass;

    g_return_if_fail(object != NULL);
    g_return_if_fail(GTK_IS_ICONTABLE(object));

    itable = GTK_ICONTABLE(object);

    /* Free icontableDir pathname */
    if (itable->icontableDir) 
    {
        free(itable->icontableDir);
        itable->icontableDir = NULL;
    }

    klass = gtk_type_class(gtk_widget_get_type());
    if (GTK_OBJECT_CLASS(klass)->destroy) {
        (* GTK_OBJECT_CLASS(klass)->destroy) (object);
    }
}

/*
 *========================================================================
 * Name:   gtk_icontable_change_page
 * Prototype:  void gtk_icontable_change_page( GtkWidget *, guint offset )
 *
 * Description:
 * Move the icon table forward or back one page.
 * Attempts to move past the start or end of the list are clamped.
 *
 * Arguments:
 * GtkWidget *          The icontable widget.
 *========================================================================
 */
static void
gtk_icontable_change_page( GtkWidget *widget, guint offset )
{
    GtkIcontable     *itable = GTK_ICONTABLE(widget);
    GdkRegion        *region;

    // TBD

    gtk_icontable_paint(widget);
    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_icontable_change_cell
 * Prototype:  void gtk_icontable_change_cell( GtkWidget *, guint offset )
 *
 * Description:
 * Move the active cell forward or back "offset" cells.
 * Attempts to move past the start or end of the list are clamped.
 *
 * Arguments:
 * GtkWidget *          The icontable widget.
 *========================================================================
 */
static void
gtk_icontable_change_cell( GtkWidget *widget, gint offset, guint highlight )
{
    GtkIcontable    *itable = GTK_ICONTABLE(widget);
    GdkRegion       *region;
    gint            index;
    guint           doPaint;
    guint           page, page_index, row, col;
    GSList          *cell;

    if ( highlight )
    {
        piboxLogger(LOG_TRACE1, "Highlight set: offset = %d.\n", offset);
        index = itable->index + offset;
        if (index < 0 )
        {
            index = 0;
            piboxLogger(LOG_TRACE1, "Resetting index to 0.\n");
        }
        else if (index >= itable->list_length )
        {
            index = itable->list_length-1;
            piboxLogger(LOG_TRACE1, "Resetting index to max length: %d.\n", itable->list_length-1);
        }
    }
    else
    {
        piboxLogger(LOG_TRACE1, "Using existing index.\n");
        index = itable->index;
    }

    /* Compute new positions */
    page = index / (itable->rows * itable->cols);
    page_index = index -  (page*(itable->rows * itable->cols));
    row = page_index / itable->cols;
    col = page_index - (row * itable->cols);

    if ( highlight )
    {
        if ( itable->page != page )
            doPaint = 1;
        itable->index = index;
        itable->page = page;
        piboxLogger(LOG_TRACE1, "Updated index %d, page %d, row x col: %d x %d\n", index, page, row, col);
    }
    else
    {
        piboxLogger(LOG_TRACE1, "Current index %d, page %d, row x col: %d x %d\n", index, page, row, col);
    }

    if ( doPaint )
        gtk_icontable_paint(widget);
    do_drawing(itable, row, col, highlight);
    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 *========================================================================
 * Public API
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   gtk_icontable_set
 * Prototype:  void gtk_icontable_set( GtkWidget *, GSList * )
 *
 * Description:
 * Sets the list of icons to use for drawing the icon table.
 * 
 * Arguments:
 * GtkWidget *widget        The icontable widget.
 * GSList    *ilist         The list of icons.
 *========================================================================
 */
void
gtk_icontable_set( GtkWidget *widget, GSList *ilist )
{
    GtkIcontable *itable = GTK_ICONTABLE(widget);
    GSList       *cell = NULL;
    guint        idx=0;

    itable->icons = ilist;
    itable->list_length = g_slist_length(ilist);
    piboxLogger(LOG_TRACE1, "Table rowsxcols: %dx%d\n", itable->rows,itable->cols);
    piboxLogger(LOG_TRACE1, "Data elements: %d\n", itable->list_length);
}

/*
 *========================================================================
 * Name:   gtk_icontable_set_icondir
 * Prototype:  void gtk_icontable_set_icondir( GtkWidget *, char * )
 *
 * Description:
 * Sets the list of icons to use for drawing the icon table.
 * 
 * Arguments:
 * GtkWidget *widget        The icontable widget.
 * char      *dir           The directory to find icons for the table.
 *========================================================================
 */
void
gtk_icontable_set_icondir( GtkWidget *widget, char *dir )
{
    GtkIcontable *itable = GTK_ICONTABLE(widget);
    if ( itable->icontableDir )
        free(itable->icontableDir);
    itable->icontableDir = strdup( dir );
}

/*
 *========================================================================
 * Name:   gtk_icontable_press
 * Prototype:  void gtk_icontable_press( GtkWidget *, int, int, int )
 *
 * Description:
 * Called with the icontable window relative position of the icon that is to
 * be pressed and release.  This is expected to be by touchscreen but
 * can also be done by mouse clicks.
 * 
 * Arguments:
 * GtkWidget *widget        The icontable widget.
 * int       x              The x (col) position, relative to the icontable window.
 * int       y              The y (row) position, relative to the icontable window.
 * int       highlight      Whether to highlight the icon border or not.
 *========================================================================
 */
void
gtk_icontable_press( GtkWidget *widget, int x, int y, int highlight )
{
    GtkIcontable     *itable;
    GdkRegion       *region;
    int             i;
    int             page_index, row, col;
    GSList          *cell;

    itable = GTK_ICONTABLE ( widget );

    /* If no index provided, redraw all the icons.  Common for first time map/expose events. */
    if ( (x == -1) && (y == -1) )
    {
        gtk_icontable_paint(widget);
    }
    else
    {
        /* Convert widget coordinates to row/col */
        row = (int)(y / itable->cellHeight);
        col = (int)(x / itable->cellWidth);

        /* Update the specific icon that's been pressed. */
        if ( (row < itable->rows) && (col < itable->cols) )
        {
            // Make this cell the active cell
            piboxLogger(LOG_TRACE1, "Old position: %d\n", itable->index);
            page_index = (row * itable->cols) + col;
            itable->index = (itable->page * (itable->rows * itable->cols)) + page_index;
            piboxLogger(LOG_TRACE1, "New index: %d, row %d, col %d\n", itable->index, row, col);

            do_drawing(itable, row, col, highlight);
        }    
        else
            piboxLogger(LOG_ERROR, "Invalid icontable position: %d,%d\n", row, col);
    }

    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_icontable_get_cell
 * Prototype:  GSList *gtk_icontable_get_cell( GtkWidget * )
 *
 * Description:
 * Retrieve the current active icon, which was set by the last 
 * call to gtk_icontable_press().  The icon is retrieved based
 * on the row/col index.
 * 
 * Arguments:
 * GtkWidget *          The icontable widget.
 *
 * Returns:
 * The string associated with the icon.  The caller is responsible
 * for freeing the returned buffer.  If the active cell does not
 * have a corresponding "label" or "shiftlabel" (as appropriate)
 * then NULL is returned.
 *========================================================================
 */
GSList *
gtk_icontable_get_cell( GtkWidget *widget )
{
    GtkIcontable     *itable = GTK_ICONTABLE(widget);
    int              row, col;
    GSList           *cell;

    cell = getCell(itable, itable->index);
    if ( cell == NULL )
    {
        piboxLogger(LOG_TRACE1, "No such icon at index %d\n", (int)(row*itable->cols)+col);
        return NULL;
    }
    return cell;
}

/*
 *========================================================================
 * Name:   gtk_icontable_next_cell
 * Prototype:  void gtk_icontable_next_cell( GtkWidget *, guint highlight )
 *
 * Description:
 * Move to the next cell or release highlight of updated cell.
 * Attempts to move past the start or end of the list are clamped.
 *
 * Arguments:
 * GtkWidget *          The icontable widget.
 *========================================================================
 */
void
gtk_icontable_next_cell( GtkWidget *widget, guint highlight )
{
    gtk_icontable_change_cell( widget, 1, highlight );
}

/*
 *========================================================================
 * Name:   gtk_icontable_prev_cell
 * Prototype:  void gtk_icontable_prev_cell( GtkWidget * )
 *
 * Description:
 * Move to the prev cell.
 * Attempts to move past the start or end of the list are clamped.
 *
 * Arguments:
 * GtkWidget *          The icontable widget.
 *========================================================================
 */
void
gtk_icontable_prev_cell( GtkWidget *widget, guint highlight )
{
    gtk_icontable_change_cell( widget, -1, highlight );
}

/*
 *========================================================================
 * Name:   gtk_icontable_next_row
 * Prototype:  void gtk_icontable_next_row( GtkWidget *, guint highlight )
 *
 * Description:
 * Move to the next row or release highlight of updated row.
 * Attempts to move past the start or end of the list are clamped.
 *
 * Arguments:
 * GtkWidget *          The icontable widget.
 *========================================================================
 */
void
gtk_icontable_next_row( GtkWidget *widget, guint highlight )
{
    GtkIcontable     *itable = GTK_ICONTABLE(widget);

    gtk_icontable_change_cell( widget, itable->cols, highlight );
}

/*
 *========================================================================
 * Name:   gtk_icontable_prev_row
 * Prototype:  void gtk_icontable_prev_row( GtkWidget * )
 *
 * Description:
 * Move to the prev row.
 * Attempts to move past the start or end of the list are clamped.
 *
 * Arguments:
 * GtkWidget *          The icontable widget.
 *========================================================================
 */
void
gtk_icontable_prev_row( GtkWidget *widget, guint highlight )
{
    GtkIcontable     *itable = GTK_ICONTABLE(widget);

    gtk_icontable_change_cell( widget, -1 * itable->cols, highlight );
}

/*
 *========================================================================
 * Name:   gtk_icontable_next_page
 * Prototype:  void gtk_icontable_next_page( GtkWidget *, guint highlight )
 *
 * Description:
 * Move to the next page or release highlight of updated page.
 * Attempts to move past the start or end of the list are clamped.
 *
 * Arguments:
 * GtkWidget *          The icontable widget.
 *========================================================================
 */
void
gtk_icontable_next_page( GtkWidget *widget, guint highlight )
{
    GtkIcontable     *itable = GTK_ICONTABLE(widget);

    gtk_icontable_change_cell( widget, itable->rows*itable->cols, highlight );
}

/*
 *========================================================================
 * Name:   gtk_icontable_prev_page
 * Prototype:  void gtk_icontable_prev_page( GtkWidget * )
 *
 * Description:
 * Move to the prev page.
 * Attempts to move past the start or end of the list are clamped.
 *
 * Arguments:
 * GtkWidget *          The icontable widget.
 *========================================================================
 */
void
gtk_icontable_prev_page( GtkWidget *widget, guint highlight )
{
    GtkIcontable     *itable = GTK_ICONTABLE(widget);

    gtk_icontable_change_cell( widget, -1 * itable->rows*itable->cols, highlight );
}

/*
 *========================================================================
 * Name:   gtk_icontable_set_cellpad
 * Prototype:  void gtk_icontable_set_cellpad( GtkWidget *, guint )
 *
 * Description:
 * Sets the padding around icons in their cells.
 * 
 * Arguments:
 * GtkWidget *widget        The icontable widget.
 * guint     pad            The pad to use around icons.
 *========================================================================
 */
void
gtk_icontable_set_cellpad( GtkWidget *widget, guint pad )
{
    GdkRegion   *region;
    GtkIcontable *itable = GTK_ICONTABLE(widget);
    itable->cell_pad = pad;
    piboxLogger(LOG_TRACE1, "Cell_pad: %d\n", itable->cell_pad );
    gtk_icontable_paint(widget);
    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_icontable_show_label
 * Prototype:  void gtk_icontable_show_label( GtkWidget *, gboolean )
 *
 * Description:
 * Determines if the label should be displayed.
 * 
 * Arguments:
 * GtkWidget *widget        The icontable widget.
 * gboolean  state          Either TRUE or FALSE.
 *========================================================================
 */
void
gtk_icontable_show_label( GtkWidget *widget, gboolean state )
{
    GdkRegion   *region;
    GtkIcontable *itable = GTK_ICONTABLE(widget);
    itable->showLabel = state;
    gtk_icontable_paint(widget);
    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_icontable_set_label_color
 * Prototype:  void gtk_icontable_set_label_color( GtkWidget *, GdkColor )
 *
 * Description:
 * Determines if the label should be displayed.
 * 
 * Arguments:
 * GtkWidget *widget        The icontable widget.
 * GdkColor  color          Color for labels.
 *========================================================================
 */
void
gtk_icontable_set_label_color( GtkWidget *widget, GdkColor color )
{
    GdkRegion   *region;
    GtkIcontable *itable = GTK_ICONTABLE(widget);
    itable->labelColor = color;
    gtk_icontable_paint(widget);
    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

