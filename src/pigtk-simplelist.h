/*******************************************************************************
 * pigtk-simplelist
 *
 * pigtk-simplelist.h:  custom widget for displaying single line of text.
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef __SIMPLELIST_H
#define __SIMPLELIST_H

#include <gtk/gtk.h>
#include <cairo.h>

G_BEGIN_DECLS

#define GTK_SIMPLELIST(obj) GTK_CHECK_CAST(obj, gtk_simplelist_get_type (), GtkSimplelist)
#define GTK_SIMPLELIST_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_simplelist_get_type(), GtkSimplelistClass)
#define GTK_IS_SIMPLELIST(obj) GTK_CHECK_TYPE(obj, gtk_simplelist_get_type())

typedef struct _GtkSimplelist GtkSimplelist;
typedef struct _GtkSimplelistClass GtkSimplelistClass;

struct _GtkSimplelist {
  GtkDrawingArea parent;
  GdkPixmap *pixmap;
  GdkColor bg;
  GdkColor fg;
  guint fontSize;
  GSList *entries;
  guint rows;
  guint offset;
  PangoAlignment alignment;
  gint active_row;
};

struct _GtkSimplelistClass {
  GtkDrawingAreaClass parent_class;
};

/* Prototypes */
GtkType gtk_simplelist_get_type(void);
GtkWidget *gtk_simplelist_new();
void gtk_simplelist_set( GtkWidget *widget, GSList *entries );
char *gtk_simplelist_get_row( GtkWidget *widget, guint x, guint y );
void gtk_simplelist_set_fontsize( GtkWidget *widget, guint size );
void gtk_simplelist_set_rows( GtkWidget *widget, guint rows );
guint gtk_simplelist_get_rows( GtkWidget *widget );
guint gtk_simplelist_get_offset( GtkWidget *widget );
void gtk_simplelist_set_offset( GtkWidget *widget, guint  );
void gtk_simplelist_press( GtkWidget *widget, int x, int y, int highlight );
void gtk_simplelist_set_alignment( GtkWidget *widget, PangoAlignment adj );

G_END_DECLS

#endif /* __SIMPLELIST_H */
