/*******************************************************************************
 * pigtk-textline
 *
 * pigtk-textline.h:  custom widget for displaying single line of text.
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 * 
 * Based on:
 * Custom GTK+ widget - http://zetcode.com/tutorials/gtktutorial/customwidget/
 * Clock Widget - https://github.com/humbhenri/clocks/tree/master/gtk-clock
 ******************************************************************************/
#define TEXTLINEWIDGET_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <glib.h>
#include <pibox/log.h>
#include "pigtk-textline.h"

#define FONT_SIZE    12

/*
 *========================================================================
 * Prototypes
 *========================================================================
 */
static void gtk_textline_class_init(GtkTextlineClass *klass);
static void gtk_textline_init(GtkTextline *textline);
static void gtk_textline_size_request(GtkWidget *widget, GtkRequisition *requisition);
static void gtk_textline_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void gtk_textline_realize(GtkWidget *widget);
static gboolean gtk_textline_expose(GtkWidget *widget, GdkEventExpose *event);
static void gtk_textline_paint(GtkWidget *widget);
static void gtk_textline_destroy(GtkObject *object);

/*
 *========================================================================
 *========================================================================
 * Private API
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:    gtk_textlineget_type
 * Prototype:   void gtk_textlineget_type( void )
 *
 * Description:
 * Returns the GtkType for this widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
GtkType
gtk_textline_get_type(void)
{
    static GtkType gtk_textline_type = 0;
    if (!gtk_textline_type) {
        static const GtkTypeInfo gtk_textline_info = {
            "GtkTextline",
            sizeof(GtkTextline),
            sizeof(GtkTextlineClass),
            (GtkClassInitFunc) gtk_textline_class_init,
            (GtkObjectInitFunc) gtk_textline_init,
            NULL,
            NULL,
            (GtkClassInitFunc) NULL
        };
        gtk_textline_type = gtk_type_unique(GTK_TYPE_WIDGET, &gtk_textline_info);
    }
    return gtk_textline_type;
}

/*
 *========================================================================
 * Name:    gtk_textline_new
 * Prototype:   void gtk_textline_new( void )
 *
 * Description:
 * Create the gtk_textline instance.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
GtkWidget * gtk_textline_new()
{
    return GTK_WIDGET(gtk_type_new(gtk_textline_get_type()));
}

/*
 *========================================================================
 * Name:   gtk_textline_class_init
 * Prototype:  void gtk_textline_class_init( GtkTextlineClass * )
 *
 * Description:
 * Initialize the widget class structure fields and setup signals.
 * 
 * Arguments:
 * GtkTextlineClass *      The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_textline_class_init(GtkTextlineClass *klass)
{
    GtkWidgetClass *widget_class;
    GtkObjectClass *object_class;

    widget_class = (GtkWidgetClass *) klass;
    object_class = (GtkObjectClass *) klass;

    widget_class->realize = gtk_textline_realize;
    widget_class->size_request = gtk_textline_size_request;
    widget_class->size_allocate = gtk_textline_size_allocate;
    widget_class->expose_event = gtk_textline_expose;
    object_class->destroy = gtk_textline_destroy;
}

/*
 *========================================================================
 * Name:   gtk_textline_init
 * Prototype:  void gtk_textline_init( GtkTextline * )
 *
 * Description:
 * Initialize the widget object and create composite window components.
 * The textline widget is not a composite, however.  It's just a giant
 * pixmap we draw in.
 * 
 * Arguments:
 * GtkTextline *      The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_textline_init(GtkTextline *textline)
{
    textline->pixmap = NULL;
    textline->field = NULL;
    textline->fontSize = FONT_SIZE;
    textline->maxLength = 1;
    textline->hidden = 0;
    textline->borderColor.red = 0;
    textline->borderColor.green = 0;
    textline->borderColor.blue = 0;
}

/*
 *========================================================================
 * Name:   gtk_textline_size_request
 * Prototype:  void gtk_textline_size_request( GtkWidget *, GtkRequisition * )
 *
 * Description:
 * Tell GTK+ what size the widget should be, initially.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkRequisition *     The widgets requested size to pass back to caller.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_textline_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_TEXTLINE(widget));
    g_return_if_fail(requisition != NULL);
}

/*
 *========================================================================
 * Name:   gtk_textline_size_allocate
 * Prototype:  void gtk_textline_size_allocate( GtkWidget *, GtkAllocation * )
 *
 * Description:
 * Handle widget resizes.  Basically this means remove the reference to the
 * pixmap (which should free it), reset the size for the widget, then
 * reallocate and paint in the pixmap at the new size.  This will expand
 * the key cells but not change the size of the font drawn in each cell.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkAllocation *      The size allocated for the widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_textline_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
    GtkTextline *textline; 

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_TEXTLINE(widget));
    g_return_if_fail(allocation != NULL);

    textline = GTK_TEXTLINE(widget);
    widget->allocation = *allocation;
    piboxLogger(LOG_TRACE1, "widget->allocation.w/h: %d/%d\n", widget->allocation.width, widget->allocation.height);
    if ( textline->pixmap != NULL )
    {
        g_object_unref(textline->pixmap);
        textline->pixmap = NULL;
    }
    if (GTK_WIDGET_REALIZED(widget)) {
        gdk_window_move_resize(
            widget->window,
            allocation->x, allocation->y,
            allocation->width, allocation->height
        );
    }
}

/*
 *========================================================================
 * Name:   gtk_textline_realize
 * Prototype:  void gtk_textline_realize( GtkWidget * )
 *
 * Description:
 * After creating the window, we set its style and background, and put a
 * pointer to the widget in the user data field of the GdkWindow. This
 * last step allows GTK to dispatch events for this window to the correct
 * widget. 
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_textline_realize(GtkWidget *widget)
{
    GdkWindowAttr attributes;
    guint attributes_mask;
    GtkStyle *style;
    GtkAllocation alloc;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_TEXTLINE(widget));

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    gtk_widget_get_allocation(widget, &alloc);

    piboxLogger(LOG_TRACE1, "alloc.x: %d\n", alloc.x);
    piboxLogger(LOG_TRACE1, "alloc.y: %d\n", alloc.y);
    piboxLogger(LOG_TRACE1, "alloc.w: %d\n", alloc.width);
    piboxLogger(LOG_TRACE1, "alloc.h: %d\n", alloc.height);

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x           = alloc.x;
    attributes.y           = alloc.y;
    attributes.width       = alloc.width;
    attributes.height      = alloc.height;
    attributes.wclass      = GDK_INPUT_OUTPUT;
    attributes.event_mask  = gtk_widget_get_events(widget) | GDK_EXPOSURE_MASK;
    attributes_mask        = GDK_WA_X | GDK_WA_Y;

    widget->window = gdk_window_new(
        gtk_widget_get_parent_window (widget),
        & attributes, attributes_mask
    );

    style = gtk_widget_get_style (widget);
    if (style != NULL) 
    {
        GTK_TEXTLINE(widget)->bg = style->bg[GTK_STATE_NORMAL];
        GTK_TEXTLINE(widget)->fg = style->fg[GTK_STATE_NORMAL];
    }
    gtk_textline_paint(widget);

    gdk_window_set_user_data(widget->window, widget);
    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
}

/*
 *========================================================================
 * Name:   gtk_textline_expose
 * Prototype:  void gtk_textline_expose( GtkWidget *, GdkEventExpose * )
 *
 * Description:
 * All drawing is done in an off-screen pixmap.  This function copies
 * that pixmap to the widget window to make the updates visible.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkRequisition *     The widgets requested size to pass back to caller.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static gboolean
gtk_textline_expose(GtkWidget *widget, GdkEventExpose *event)
{
    int width, height;

    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_TEXTLINE(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);

    // Update the the widget.
    gtk_textline_paint(widget);

    // Copy the entire pixmap over the widget's drawing area.
    if ( GTK_TEXTLINE(widget)->pixmap != NULL )
    {
        piboxLogger(LOG_TRACE1, "widget->w/h: %d/%d\n", widget->allocation.width, widget->allocation.height);
        gdk_drawable_get_size(GTK_TEXTLINE(widget)->pixmap, &width, &height);
        piboxLogger(LOG_TRACE1, "pixmap w/h: %d/%d\n", width, height);

        gdk_draw_drawable(widget->window,
            widget->style->fg_gc[GTK_WIDGET_STATE(widget)], GTK_TEXTLINE(widget)->pixmap,
            event->area.x, event->area.y,
            event->area.x, event->area.y,
            event->area.width, event->area.height);
    }
    return TRUE;
}

/*
 *========================================================================
 * Name:    gtk_textline_paint
 * Prototype:   void gtk_textline_paint( GtkWidget *widget )
 *
 * Description:
 * Handle painting (data update) of the widget.
 *========================================================================
 */
static void
gtk_textline_paint(GtkWidget *widget)
{
    GtkTextline             *tline;
    int                     width, height;
    int                     i, len;
    cairo_t                 *cr_pixmap;
    cairo_surface_t         *cst;
    cairo_t                 *cr;
    GdkPixmap               *pixmap;
    static int              painting = 0;

    PangoFontDescription    *desc;
    PangoRectangle          pangoRectangle;
    PangoAttrList           *attrs = NULL;
    PangoLayout             *layout;
    char                    buf[256];
    char                    *ptr, *hiddenStr;

    /* Avoid multiple calls */
    if ( painting )
        return;

    painting = 1;

    if ( GTK_TEXTLINE(widget)->pixmap == NULL )
    {   
        GTK_TEXTLINE(widget)->pixmap = 
            gdk_pixmap_new(widget->window,widget->allocation.width,widget->allocation.height,-1);
    }
    pixmap = GTK_TEXTLINE(widget)->pixmap;
    if ( pixmap == NULL )
    {
        piboxLogger(LOG_ERROR, "No pixmap for widget!\n");
        return;
    }
    piboxLogger(LOG_TRACE1, "Allocation w/h: %d, %d\n", widget->allocation.width, widget->allocation.height);

    piboxLogger(LOG_TRACE2, "Getting cr\n");
    gdk_drawable_get_size(pixmap, &width, &height);
    piboxLogger(LOG_TRACE2, "Drawable w/h: %d, %d\n", width, height);
    cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    cr = cairo_create(cst);

    // Fill background with white
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_fill(cr);

    // Draw dark outline
    tline = GTK_TEXTLINE(widget);
    cairo_set_source_rgb(cr, 
        tline->borderColor.red/65535, tline->borderColor.green/65535, tline->borderColor.blue/65535);
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_move_to(cr, 0, 0);
    cairo_line_to(cr, (double)width, 0);
    cairo_line_to(cr, (double)width, (double)height);
    cairo_line_to(cr, 0, (double)height);
    cairo_line_to(cr, 0, 0);
    cairo_set_line_width(cr, 1);
    cairo_stroke(cr); 


    layout = pango_cairo_create_layout(cr);
    sprintf(buf, "Nunito Bold %d", GTK_TEXTLINE(widget)->fontSize);
    desc = pango_font_description_from_string( buf );
    pango_font_description_set_absolute_size(desc, GTK_TEXTLINE(widget)->fontSize*PANGO_SCALE);
    pango_layout_set_font_description(layout, desc);
    pango_font_description_free(desc);
    pango_layout_set_width(layout, -1); // Disable wrap
    pango_layout_set_spacing(layout, 1);
    pango_layout_set_single_paragraph_mode(layout, FALSE);
    pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);

    /* Initialize attributes. */
    attrs = pango_attr_list_new();
    pango_attr_list_insert (attrs, pango_attr_underline_new(PANGO_UNDERLINE_NONE));
    pango_layout_set_attributes (layout, attrs);

    piboxLogger(LOG_TRACE1, "hidden state: %d\n", GTK_TEXTLINE(widget)->hidden);
    if ( GTK_TEXTLINE(widget)->field )
    {
        if ( GTK_TEXTLINE(widget)->hidden )
        {
            piboxLogger(LOG_TRACE1, "Drawing hidden text.\n");
            len = strlen(GTK_TEXTLINE(widget)->field);
            ptr = hiddenStr = (char *)calloc(1, strlen(GTK_TEXTLINE(widget)->field) + 1);
            for (i=0; i<len; i++ )
                *ptr++ = '*';
            pango_layout_set_text(layout, hiddenStr, -1);
            free(hiddenStr);
        }
        else
        {
            piboxLogger(LOG_TRACE1, "text: %s\n", GTK_TEXTLINE(widget)->field);
            pango_layout_set_text(layout, GTK_TEXTLINE(widget)->field, -1);
        }
    }
    else
    {
        piboxLogger(LOG_TRACE1, "text: NULL\n");
        pango_layout_set_text(layout, " ", -1);
    }
    pango_layout_get_pixel_size(layout, &pangoRectangle.width, &pangoRectangle.height );
    piboxLogger(LOG_TRACE4, "text w/h: %d / %d\n", pangoRectangle.width, pangoRectangle.height);
    cairo_set_source_rgb(cr, 0, 0, 0);
    pango_cairo_update_layout(cr, layout);

    /* Don't need to center horizontally because pango_layout_set_alignment does that for us. */
    cairo_translate(cr, 0, (height-(pangoRectangle.height))/2);
    pango_cairo_show_layout(cr, layout);

    /* Cleanup pango */
    pango_attr_list_unref(attrs);
    g_object_unref(layout);

    piboxLogger(LOG_TRACE1, "Paint the pixmap.\n");
    cr_pixmap = gdk_cairo_create(pixmap);
    cairo_set_source_surface (cr_pixmap, cst, 0, 0);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);

    /* Don't need the cairo object now */
    cairo_surface_destroy(cst);
    cairo_destroy(cr);

    piboxLogger(LOG_TRACE1, "Done - Paint the pixmap.\n");
    painting = 0;
}

/*
 *========================================================================
 * Name:   gtk_textline_destroy
 * Prototype:  void gtk_textline_destroy( GtkObject * )
 *
 * Description:
 * Clean up the widget when it is removed.  This is only called,
 * at least for this widget, when the application window is closed.
 * 
 * Arguments:
 * GtkObject *          The object class of the widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_textline_destroy(GtkObject *object)
{
    // GtkTextline *textline;
    GtkTextlineClass *klass;

    g_return_if_fail(object != NULL);
    g_return_if_fail(GTK_IS_TEXTLINE(object));

    // textline = GTK_TEXTLINE(object);
    klass = gtk_type_class(gtk_widget_get_type());
    if (GTK_OBJECT_CLASS(klass)->destroy) {
        (* GTK_OBJECT_CLASS(klass)->destroy) (object);
    }
}

/*
 *========================================================================
 *========================================================================
 * Public API
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   gtk_textline_set
 * Prototype:  void gtk_textline_set( GtkWidget *, int )
 *
 * Description:
 * Update the widget text with a new character (or string of characters).
 * 
 * Arguments:
 * GtkWidget *widget        The textline widget.
 * int       highlight      Whether to highlight the key border or not.
 *========================================================================
 */
void
gtk_textline_set( GtkWidget *widget, char *field )
{
    GdkRegion       *region;
    GtkTextline     *textline;

    if ( field == NULL )
        return;
    textline = GTK_TEXTLINE(widget);
    if (strlen(field) > textline->maxLength )
    {
        piboxLogger(LOG_ERROR, 
            "Request update is too long (maxLength: %d): %s\n", textline->maxLength, field);
        return;
    }

    /*
     * Retrieve data for use in updating the scan widget.
     */
    piboxLogger(LOG_TRACE1, "New field: %s\n", field);
    if ( textline->field )
        free(textline->field);
    textline->field = strdup(field);
    gtk_textline_paint(widget);

    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_textline_get
 * Prototype:  void gtk_textline_get( GtkWidget * )
 *
 * Description:
 * Retrieve the current widget text.  Caller is responsible for freeing
 * the returned string.
 * 
 * Arguments:
 * GtkWidget *widget        The textline widget.
 *========================================================================
 */
char *
gtk_textline_get( GtkWidget *widget )
{
    GtkTextline     *textline;

    textline = GTK_TEXTLINE(widget);
    if ( textline->field )
        return strdup(textline->field);
    else
        return NULL;
}

/*
 *========================================================================
 * Name:   gtk_textline_set_fontsize
 * Prototype:  void gtk_textline_set_fontsize( GtkWidget *, guint )
 *
 * Description:
 * Set the size of the font text.  Should be called before the widget is
 * realized.
 * 
 * Arguments:
 * GtkWidget *widget        The textline widget.
 *
 * Notes:
 * This should be handled with the gtkrc, but this was the simplest way
 * to implement it for now.
 *========================================================================
 */
void
gtk_textline_set_fontsize( GtkWidget *widget, guint size )
{
    GdkRegion       *region;
    GtkTextline     *textline;

    textline = GTK_TEXTLINE(widget);
    textline->fontSize = size;
}

/*
 *========================================================================
 * Name:   gtk_textline_get_hidden
 * Prototype:  guint gtk_textline_get_hidden( GtkWidget * )
 *
 * Description:
 * Retrieve hidden state.
 * 
 * Arguments:
 * GtkWidget *widget        The textline widget.
 *
 * Returns:
 * 1 if hidden state is enabled.
 * 0 if hidden state is not enabled.
 *========================================================================
 */
guint
gtk_textline_get_hidden( GtkWidget *widget )
{
    GtkTextline     *textline;

    textline = GTK_TEXTLINE(widget);
    return( textline->hidden );
}

/*
 *========================================================================
 * Name:   gtk_textline_set_hidden
 * Prototype:  void gtk_textline_set_hidden( GtkWidget *, guint )
 *
 * Description:
 * Enable (1) or disable (0) hidden mode for the display.
 * 
 * Arguments:
 * GtkWidget *widget        The textline widget.
 * guint     hidden         The hidden state.
 *========================================================================
 */
void
gtk_textline_set_hidden( GtkWidget *widget, guint hidden )
{
    GdkRegion       *region;
    GtkTextline     *textline;

    textline = GTK_TEXTLINE(widget);
    textline->hidden = hidden;

    gtk_textline_paint(widget);
    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_textline_set_border
 * Prototype:  void gtk_textline_set_border( GtkWidget *, GdkColor *color )
 *
 * Description:
 * Set the border color.
 * 
 * Arguments:
 * GtkWidget *widget        The textline widget.
 * GdkColor  *color         The color to use for the border.
 *========================================================================
 */
void
gtk_textline_set_border( GtkWidget *widget, GdkColor *color )
{
    GdkRegion       *region;
    GtkTextline     *textline;

    textline = GTK_TEXTLINE(widget);
    textline->borderColor.red = color->red;
    textline->borderColor.green = color->green;
    textline->borderColor.blue = color->blue;

    gtk_textline_paint(widget);
    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_textline_set_maxlength
 * Prototype:  void gtk_textline_set_maxlength( GtkWidget *, guint maxLength )
 *
 * Description:
 * Set the border color.
 * 
 * Arguments:
 * GtkWidget *widget        The textline widget.
 * guint     *maxlength     The maximum length of the widget; defaults to 1
 *========================================================================
 */
void
gtk_textline_set_maxlength( GtkWidget *widget, guint maxLength )
{
    GdkRegion       *region;
    GtkTextline     *textline;

    textline = GTK_TEXTLINE(widget);
    textline->maxLength = maxLength;
}
