/*******************************************************************************
 * pigtk test application
 *
 * cli.h:  Command line parsing
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef CLI_H
#define CLI_H

/*========================================================================
 * Type definitions
 *=======================================================================*/
#define MAXBUF          4096
#define CLI_LOGTOFILE   0x00001    // Enable log to file
#define CLI_TEST        0x00002    // Enable test mode
#define F_GTKRC         "data/gtkrc"

typedef struct _cli_t {
    int     flags;          // Enable/disable features
    char    *logFile;       // Name of local file to write log to
    int     verbose;        // Sets the verbosity level for the application
    char    *gtkrc;         // The GTK RC file to use.
    GSList  *slist_data1;   // Sample data for the Simplelist widget.
    GSList  *slist_data2;   // Sample data for the Simplelist widget.
    GSList  *icons;         // Sample icons for the Icontable widget.
    int     geometryW;
    int     geometryH;
} CLI_T;


/*========================================================================
 * Text strings 
 *=======================================================================*/

/* Program name for display purposes */
#define PROGNAME    "pigtk"

/* Version information should be passed from the build */
#ifndef VERSTR
#define VERSTR      "No Version String"
#endif

#ifndef VERDATE
#define VERDATE     "No Version Date"
#endif

#define CLIARGS     "eTg:l:v:"
#define USAGE \
"\n\
pigtk [ -T | -g wxh | -l <filename> | -v <level> | -h? ]\n\
where\n\
\n\
    -T              Run in test mode. \n\
    -g wxh          Set geometry to width x height. \n\
    -l filename     Enable local logging to named file \n\
    -v level        Enable verbose output: \n\
                    0: LOG_NONE  (default) \n\
                    1: LOG_INFO            \n\
                    2: LOG_WARN            \n\
                    3: LOG_ERROR           \n\
                    4: LOG_TRACE1          \n\
                    5: LOG_TRACE2          \n\
                    6: LOG_TRACE3          \n\
                    7: LOG_TRACE4          \n\
                    8: LOG_TRACE5          \n\
    Environment variables \n\
    GTK_KEYBOARD_DIR        If set, use this keyboard configuration file. \n\
                            Default:  /usr/share/gtk-keyboard. \n\
    GTK_ICONTABLE_DIR       If set, use this directory for icons to use in icontable widget. \n\
                            Default: /usr/share/gtk-icontable \n\
\n\
"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef CLI_C
CLI_T cliOptions;
#else
extern CLI_T cliOptions;
#endif /* CLI_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifdef CLI_H
void parseArgs(int argc, char **argv);
void initConfig( void );
int  isCLIFlagSet( int bits );
void setCLIFlag( int bits );
void unsetCLIFlag( int bits );
#else
extern void parseArgs(int argc, char **argv);
extern void initConfig( void );
extern int  isCLIFlagSet( int bits );
extern void setCLIFlag( int bits );
extern void unsetCLIFlag( int bits );
#endif

#endif /* !CLI_H */
