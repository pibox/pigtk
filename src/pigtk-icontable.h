/*******************************************************************************
 * pigtk widget library
 *
 * pigtk-icontable.c:  Icontable widget
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef __ICONTABLEWIDGET_H
#define __ICONTABLEWIDGET_H

#include <gtk/gtk.h>
#include <cairo.h>
#include <pibox/parson.h>

G_BEGIN_DECLS

#define GTK_ICONTABLE(obj) GTK_CHECK_CAST(obj, gtk_icontable_get_type (), GtkIcontable)
#define GTK_ICONTABLE_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_icontable_get_type(), GtkIcontableClass)
#define GTK_IS_ICONTABLE(obj) GTK_CHECK_TYPE(obj, gtk_icontable_get_type())

/* This can be overridden using the ICONTABLE_DIR environment variable. */
#define GTK_ICONTABLE_DIR        "/usr/share/gtk-icontable"

typedef struct _GtkIcontable GtkIcontable;
typedef struct _GtkIcontableClass GtkIcontableClass;
typedef struct _GtkIconCell GtkIconCell;

struct _GtkIconCell {
    char *icon;         /* Icon file to use.  Searched for in GTK_ICONTABLE_DIR. */
    char *highlight;    /* Image file used to highlight the icon.  If missing, use white outline of cell. */
    char *label;        /* Label to display under icon, if desired. */
    int  index;         /* Simple index set by the widget. */
    void *data;         /* User data to provide on selection of a cell. */
};

struct _GtkIcontable {
  GtkDrawingArea    parent;
  GdkPixmap         *pixmap;
  GdkColor          bg;
  GdkColor          fg;
  GdkColor          labelColor;
  char              *icontableDir;
  GSList            *icons;
  guint             index;
  guint             page;
  guint             list_length;
  int               rows;
  int               cols;
  gboolean          showLabel;
  guint             width;
  guint             height;
  guint             cellWidth;
  guint             cellHeight;
  guint             cell_pad;
  guint             col_pad;
  guint             row_pad;
};

struct _GtkIcontableClass {
  GtkDrawingAreaClass parent_class;
  void (* icontable) (GtkIcontable *kbd);
};

/* Prototypes */
GtkType gtk_icontable_get_type(void);
GtkWidget *gtk_icontable_new(guint rows, guint cols);
void gtk_icontable_set(GtkWidget *icontable, GSList *);
void gtk_icontable_press( GtkWidget *icontable, int x, int y, int highlight );
void gtk_icontable_next_page( GtkWidget *widget, guint highlight );
void gtk_icontable_prev_page( GtkWidget *widget, guint highlight );
void gtk_icontable_next_row( GtkWidget *widget, guint highlight );
void gtk_icontable_prev_row( GtkWidget *widget, guint highlight );
void gtk_icontable_next_cell( GtkWidget *widget, guint highlight);
void gtk_icontable_prev_cell( GtkWidget *widget, guint highlight);
GSList *gtk_icontable_get_cell( GtkWidget *icontable );
void gtk_icontable_set_cellpad( GtkWidget *widget, guint size );
void gtk_icontable_show_label( GtkWidget *widget, gboolean state );
void gtk_icontable_set_label_color( GtkWidget *widget, GdkColor color );

G_END_DECLS

#endif /* __ICONTABLEWIDGET_H */
