/*******************************************************************************
 * pigtk test application
 *
 * cli.c:  Command line parsing
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CLI_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <glib.h>

#include "pigtk-icontable.h"
#include "cli.h"

static pthread_mutex_t cliOptionsMutex = PTHREAD_MUTEX_INITIALIZER;

/*========================================================================
 * Name:   parseArgs
 * Prototype:  void parseArgs( int, char ** )
 *
 * Description:
 * Parse the command line
 *
 * Input Arguments:
 * int argc        Number of command line arguments
 * char **argv     Command line arguments to parse
 *========================================================================*/
void
parseArgs(int argc, char **argv)
{
    int opt;
    char *str[3];

    /* Suppress error messages from getopt_long() for unrecognized options. */
    opterr = 0;

    /* Parse the command line. */
    while ( (opt = getopt(argc, argv, CLIARGS)) != -1 )
    {
        switch (opt)
        {
            /* -g: Set geometry, for testing purposes. */
            case 'g':
                if ( index(optarg, 'x') )
                {
                    str[0] = strdup(optarg);
                    str[1] = strtok(str[0], "x");
                    str[2] = strtok(NULL, "x");
                    cliOptions.geometryW = atoi(str[1]);
                    cliOptions.geometryH = atoi(str[2]);
                    free(str[0]);
                }
                break;

            /* Enable logging to local file. */
            case 'l':
                cliOptions.flags |= CLI_LOGTOFILE;
                cliOptions.logFile = optarg;
                break;

            /* -v: Set verbosity level */
            case 'v':
                cliOptions.verbose = atoi(optarg);
                break;

            /* Enable test mode. */
            case 'T':
                cliOptions.flags |= CLI_TEST;
                break;

            default:
                printf("%s \nVersion: %s - %s\n", PROGNAME, VERSTR, VERDATE);
                printf(USAGE);
                exit(0);
                break;
        }
    }
}

/*========================================================================
 * Name:   initConfig
 * Prototype:  void initConfig( void )
 *
 * Description:
 * Initialize run time configuration.  These are the default
 * settings.
 *========================================================================*/
void
initConfig( void )
{
    struct stat     stat_buf;
    char            buf[128];
    char            *ptr;
    int             i;
    GtkIconCell     *entry;
    GSList          *cell;

    memset(&cliOptions, 0, sizeof(CLI_T));
    if ( stat(F_GTKRC, &stat_buf) == 0 )
        cliOptions.gtkrc = F_GTKRC;
    else
        fprintf(stderr, "Can't find gtkrc: %s\n", F_GTKRC);

    /* Use default geometry for the window. */
    cliOptions.geometryW = -1;
    cliOptions.geometryH = -1;

    /* Initialize a list of data for use in the Simplelist widget */
    cliOptions.slist_data1 = NULL;
    cliOptions.slist_data2 = NULL;
    for (i=0; i<10; i++)
    {
        sprintf(buf, "This is line %d.", i);
        ptr = strdup(buf);
        cliOptions.slist_data1 = g_slist_append(cliOptions.slist_data1, ptr);

        sprintf(buf, "Completely different stuff %d.", i);
        ptr = strdup(buf);
        cliOptions.slist_data2 = g_slist_append(cliOptions.slist_data2, ptr);
    }

    /* Setup cell structures for the icontable widget. */
    for (i=0; i<24; i++)
    {
        entry = (GtkIconCell *)calloc(1, sizeof(GtkIconCell));
        sprintf(buf, "Cell %d", i);
        entry->label = strdup(buf);
        entry->icon = strdup("icon.png");
        entry->highlight = strdup("highlight.png");
        entry->index = i;
        cliOptions.icons = g_slist_append(cliOptions.icons, entry);
    }

#if 0
    /* This is just to validate the icons list. */
    for (i=0; i<24; i++)
    {
        cell = g_slist_nth(cliOptions.icons, i);
        if ( cell == NULL )
        {
            fprintf(stderr, "No cell at %d\n", i);
            exit(1);
        }
        entry = (GtkIconCell *)g_slist_nth_data(cell, 0);
        if ( entry == NULL )
        {
            fprintf(stderr, "No cell data at %d\n", i);
            exit(1);
        }
    }
#endif

    fprintf(stderr, "Size of slist_data1: %d\n", g_slist_length(cliOptions.slist_data1));
    fprintf(stderr, "Size of slist_data2: %d\n", g_slist_length(cliOptions.slist_data2));
    fprintf(stderr, "Size of icons: %d\n", g_slist_length(cliOptions.icons));
}


/*========================================================================
 * Name:   isCLIFlagSet
 * Prototype:  void isCLIFlagSet( int )
 *
 * Description:
 * Checks to see if an option is set in cliOptions.flags using a thread lock.
 * 
 * Returns;
 * 0 if requested flag is not set.
 * 1 if requested flag is set.
 * 
 * Notes:
 * Thread safe.
 *========================================================================*/
int
isCLIFlagSet( int bits )
{
    int status = 0;

    pthread_mutex_lock( &cliOptionsMutex );
    if ( cliOptions.flags & bits )
        status = 1;
    pthread_mutex_unlock( &cliOptionsMutex );

    return status;
}

/*========================================================================
 * Name:   setCLIFlag
 * Prototype:  void setCLIFlag( int )
 *
 * Description:
 * Set options is in cliOptions.flags using a thread lock.
 *
 * Notes:
 * Thread safe.
 *========================================================================*/
void
setCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags |= bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

/*========================================================================
 * Name:   unsetCLIFlag
 * Prototype:  void unsetCLIFlag( int )
 *
 * Description:
 * Unset options is in cliOptions.flags using a thread lock.
 *
 * Notes:
 * Thread safe.
 *========================================================================*/
void
unsetCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags &= ~bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

