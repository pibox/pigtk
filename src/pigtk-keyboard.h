/*******************************************************************************
 * pigtk widget library
 *
 * pigtk-keyboard.c:  Keyboard widget
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef __KEYBOARDWIDGET_H
#define __KEYBOARDWIDGET_H

#include <gtk/gtk.h>
#include <cairo.h>
#include <pibox/parson.h>

G_BEGIN_DECLS

#define GTK_KEYBOARD(obj) GTK_CHECK_CAST(obj, gtk_keyboard_get_type (), GtkKeyboard)
#define GTK_KEYBOARD_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_keyboard_get_type(), GtkKeyboardClass)
#define GTK_IS_KEYBOARD(obj) GTK_CHECK_TYPE(obj, gtk_keyboard_get_type())

/* This can be overridden using the KEYBOARD_DIR environment variable. */
#define GTK_KEYBOARD_DIR        "/usr/share/gtk-keyboard"

typedef struct _GtkKeyboard GtkKeyboard;
typedef struct _GtkKeyboardClass GtkKeyboardClass;
typedef struct _GtkKeyboardCell GtkKeyboardCell;
typedef struct _GtkActiveCell GtkActiveCell;

struct _GtkActiveCell {
    int row;
    int col;
};

struct _GtkKeyboard {
  GtkDrawingArea    parent;
  GdkPixmap         *pixmap;
  GtkKeyboardCell   **cell;
  GdkColor          bg;
  GdkColor          fg;
  JSON_Value        *root_value;
  JSON_Object       *root_object;
  int               shift;
  int               rows;
  int               cols;
  guint             pages;
  guint             activePage;
  char              *keyboardDir;
  char              *keyboardFile;
  int               width;
  int               height;
  int               cellWidth;
  int               cellHeight;
  int               col_pad;
  int               row_pad;
  int               fontSize;
  GtkActiveCell     active_cell;
};

struct _GtkKeyboardClass {
  GtkDrawingAreaClass parent_class;
  void (* keyboard) (GtkKeyboard *kbd);
};

/* Prototypes */
GtkType gtk_keyboard_get_type(void);
GtkWidget *gtk_keyboard_new();
void gtk_keyboard_press( GtkWidget *keyboard, int x, int y, int highlight );
char *gtk_keyboard_get_key( GtkWidget *keyboard );
void gtk_keyboard_shift( GtkWidget *widget );
void gtk_keyboard_set_fontsize( GtkWidget *widget, guint size );
void gtk_keyboard_increment_page( GtkWidget *widget );

G_END_DECLS

#endif /* __KEYBOARDWIDGET_H */
