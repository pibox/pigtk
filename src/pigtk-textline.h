/*******************************************************************************
 * pigtk-textline
 *
 * pigtk-textline.h:  custom widget for displaying single line of text.
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef __TEXTLINE_H
#define __TEXTLINE_H

#include <gtk/gtk.h>
#include <cairo.h>

G_BEGIN_DECLS

#define GTK_TEXTLINE(obj) GTK_CHECK_CAST(obj, gtk_textline_get_type (), GtkTextline)
#define GTK_TEXTLINE_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_textline_get_type(), GtkTextlineClass)
#define GTK_IS_TEXTLINE(obj) GTK_CHECK_TYPE(obj, gtk_textline_get_type())

typedef struct _GtkTextline GtkTextline;
typedef struct _GtkTextlineClass GtkTextlineClass;

struct _GtkTextline {
  GtkDrawingArea parent;
  GdkPixmap *pixmap;
  GdkColor bg;
  GdkColor fg;
  GdkColor borderColor;
  guint init;
  gint fontSize;
  gint maxLength;
  gint hidden;
  char *field;
};

struct _GtkTextlineClass {
  GtkDrawingAreaClass parent_class;
};

/* Prototypes */
GtkType gtk_textline_get_type(void);
GtkWidget *gtk_textline_new();
void gtk_textline_set( GtkWidget *widget, char *field );
char * gtk_textline_get( GtkWidget *widget );
void gtk_textline_set_fontsize( GtkWidget *widget, guint size );
guint gtk_textline_get_hidden( GtkWidget *widget );
void gtk_textline_set_hidden( GtkWidget *widget, guint hidden );
void gtk_textline_set_border( GtkWidget *widget, GdkColor *color );
void gtk_textline_set_maxlength( GtkWidget *widget, guint maxLength );

G_END_DECLS

#endif /* __TEXTLINE_H */
