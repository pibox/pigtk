/*******************************************************************************
 * pigtk test application
 *
 * main.c:  program main
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MAIN_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pibox/log.h>

#include "cli.h"
#include "pigtk-keyboard.h"
#include "pigtk-textline.h"
#include "pigtk-simplelist.h"
#include "pigtk-icontable.h"

typedef struct _display_dimensions {
    int     width;
    int     height;
} DISPLAY_DIMENSIONS_T;

GtkWidget   *keyboard;
GtkWidget   *keyboard_entry;
GtkWidget   *slist;
GtkWidget   *slist_entry;
GSList      *active_list;
GtkWidget   *icon_entry;
GtkWidget   *icontable;
gint window_expose_id;

/*
 *========================================================================
 * Name:   icon_cell
 * Prototype:  void icon_cell( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Moves active icon selection manually.
 *========================================================================
 */
static gboolean
icon_cell(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    guint idx = GPOINTER_TO_INT(user_data);
    switch(idx)
    {
        case 0: gtk_icontable_prev_cell(icontable, 1);
                break;
        case 1: gtk_icontable_next_cell(icontable, 1);
                break;
    }
}

/*
 *========================================================================
 * Name:   icon_cell_release
 * Prototype:  void icon_cell_release( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Moves active icon selection manually.
 *========================================================================
 */
static gboolean
icon_cell_release(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    guint idx = GPOINTER_TO_INT(user_data);
    switch(idx)
    {
        case 0: gtk_icontable_prev_cell(icontable, 0);
                break;
        case 1: gtk_icontable_next_cell(icontable, 0);
                break;
    }
}

/*
 *========================================================================
 * Name:   icon_row
 * Prototype:  void icon_row( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Moves active icon selection by row manually.
 *========================================================================
 */
static gboolean
icon_row(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    guint idx = GPOINTER_TO_INT(user_data);
    switch(idx)
    {
        case 0: gtk_icontable_prev_row(icontable, 1);
                break;
        case 1: gtk_icontable_next_row(icontable, 1);
                break;
    }
}

/*
 *========================================================================
 * Name:   icon_row_release
 * Prototype:  void icon_row_release( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Moves active icon selection by row manually.
 *========================================================================
 */
static gboolean
icon_row_release(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    guint idx = GPOINTER_TO_INT(user_data);
    switch(idx)
    {
        case 0: gtk_icontable_prev_row(icontable, 0);
                break;
        case 1: gtk_icontable_next_row(icontable, 0);
                break;
    }
}

/*
 *========================================================================
 * Name:   icon_page
 * Prototype:  void icon_page( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Moves active icon selection by page manually.
 *========================================================================
 */
static gboolean
icon_page(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    guint idx = GPOINTER_TO_INT(user_data);
    switch(idx)
    {
        case 0: gtk_icontable_prev_page(icontable, 1);
                break;
        case 1: gtk_icontable_next_page(icontable, 1);
                break;
    }
}

/*
 *========================================================================
 * Name:   icon_page_release
 * Prototype:  void icon_page_release( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Moves active icon selection by page manually.
 *========================================================================
 */
static gboolean
icon_page_release(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    guint idx = GPOINTER_TO_INT(user_data);
    switch(idx)
    {
        case 0: gtk_icontable_prev_page(icontable, 0);
                break;
        case 1: gtk_icontable_next_page(icontable, 0);
                break;
    }
}

/*
 *========================================================================
 * Name:   icon_press
 * Prototype:  void icon_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches an icon press (as with a mouse).
 *========================================================================
 */
static gboolean
icon_press(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;
    char        *buf = NULL;
    GSList      *entry;
    GtkIconCell *cell;
    GdkColor    color;

    static gint toggle = 1;
    static gint labelColorToggle = 1;

    x = event->x;
    y = event->y;
    piboxLogger(LOG_TRACE1, "Position: %f x %f\n", x, y);
    // first press sets the active cell.
    gtk_icontable_press(icontable, x, y, GPOINTER_TO_INT(user_data));
    entry = gtk_icontable_get_cell(icontable);
    if ( entry )
    {
        cell = (GtkIconCell *)g_slist_nth_data(entry, 0);
        switch(cell->index)
        {
            case 0: 
                gtk_icontable_set_cellpad( icontable, 0);
                break;
            case 1: 
                gtk_icontable_set_cellpad( icontable, 50);
                break;
            case 2: 
                toggle = toggle ^ 1;
                if ( toggle )
                    gtk_icontable_show_label( icontable, TRUE);
                else
                    gtk_icontable_show_label( icontable, FALSE);
                break;
            case 3: 
                labelColorToggle = labelColorToggle ^ 1;
                if ( labelColorToggle )
                {
                    color.red = 65535;
                    color.green = 1500;
                    color.blue = 1500;
                    gtk_icontable_set_label_color( icontable, color);
                }
                else
                {
                    color.red = 65535;
                    color.green = 65535;
                    color.blue = 65535;
                    gtk_icontable_set_label_color( icontable, color);
                }
                break;
        }
    }
    // gtk_icontable_set_cellpad() redraws the whole table so we don't
    // see the highlight from the first press - so do it one more time.
    gtk_icontable_press(icontable, x, y, GPOINTER_TO_INT(user_data));
    gtk_widget_queue_draw(icontable);
}

/*
 *========================================================================
 * Name:   icon_release
 * Prototype:  void icon_release( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches an icon release (as with a mouse).
 *========================================================================
 */
static gboolean
icon_release(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;
    char        *buf = NULL;
    GSList      *entry;
    GtkIconCell cell;

    x = event->x;
    y = event->y;
    piboxLogger(LOG_TRACE1, "Position: %f x %f\n", x, y);
    gtk_icontable_press(icontable, x, y, GPOINTER_TO_INT(user_data));
    gtk_widget_queue_draw(icontable);
}

/*
 *========================================================================
 * Name:   border_toggle
 * Prototype:  void border_toggle( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Toggle the border color.
 *========================================================================
 */
static gboolean
border_toggle(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    static gint toggle = 0;
    GdkColor    dark;
    GdkColor    hilite;

    dark.red = 0;
    dark.green = 0;
    dark.blue = 0;

    hilite.red = 65535;
    hilite.green = 10000;
    hilite.blue = 10000;

    toggle = toggle ^ 1;
    if ( toggle )
        gtk_textline_set_border(slist_entry, &hilite);
    else
        gtk_textline_set_border(slist_entry, &dark);
}

/*
 *========================================================================
 * Name:   hidden_toggle
 * Prototype:  void hidden_toggle( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Toggle the hidden textline field.
 *========================================================================
 */
static gboolean
hidden_toggle(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gint toggle;
    toggle = gtk_textline_get_hidden(slist_entry);
    piboxLogger(LOG_TRACE1, "toggle: %d\n", toggle);
    toggle = toggle ^ 1;
    piboxLogger(LOG_TRACE1, "toggled: %d\n", toggle);
    gtk_textline_set_hidden(slist_entry, toggle);
}

/*
 *========================================================================
 * Name:   slist_back
 * Prototype:  void slist_back( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Move back in the slist.
 *========================================================================
 */
static gboolean
slist_back(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gint offset;
    offset = gtk_simplelist_get_offset(slist);
    piboxLogger(LOG_TRACE1, "offset: %d\n", offset);
    if ( (offset-1) >=0 )
        gtk_simplelist_set_offset(slist, offset-1);
}

/*
 *========================================================================
 * Name:   slist_forward
 * Prototype:  void slist_forward( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Move forward in the slist.
 *========================================================================
 */
static gboolean
slist_forward(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gint offset;
    gint rows;
    offset = gtk_simplelist_get_offset(slist);
    rows = gtk_simplelist_get_rows(slist);
    piboxLogger(LOG_TRACE1, "offset: %d, slist.length: %d\n", offset, g_slist_length(active_list) );
    if ( (offset+rows+1) <= (gint)g_slist_length(active_list) )
        gtk_simplelist_set_offset(slist, offset+1);
}

/*
 *========================================================================
 * Name:   slist_switch
 * Prototype:  void slist_switch( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Switch the slist.
 *========================================================================
 */
static gboolean
slist_switch(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    static guint toggle = 0;
    toggle = toggle ^ 1;
    switch(toggle)
    {
        case 0: gtk_simplelist_set(slist, cliOptions.slist_data1);
                active_list = cliOptions.slist_data1;
                break;
        case 1: gtk_simplelist_set(slist, cliOptions.slist_data2);
                active_list = cliOptions.slist_data1;
                break;
    }
    gtk_widget_queue_draw(slist);
    gtk_textline_set(slist_entry, "");
    gtk_textline_set_hidden(slist_entry, 0);
    gtk_widget_queue_draw(slist_entry);
}

/*
 *========================================================================
 * Name:   slist_press
 * Prototype:  void slist_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a press (as with a mouse) in the simplelist.
 *========================================================================
 */
static gboolean
slist_press(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;
    char        *key;
    char        *entry;

    x = event->x;
    y = event->y;
    piboxLogger(LOG_TRACE1, "Position: %f x %f\n", x, y);
    gtk_simplelist_press(slist, x, y, GPOINTER_TO_INT(user_data));
    gtk_widget_queue_draw(slist);
    entry = gtk_simplelist_get_row(slist, x, y);
    gtk_textline_set(slist_entry, entry);
    gtk_widget_queue_draw(slist_entry);

    return(TRUE);
}

/*
 *========================================================================
 * Name:   slist_release
 * Prototype:  void slist_release( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a button release (as with a mouse).
 *========================================================================
 */
static gboolean
slist_release(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;
    x = event->x;
    y = event->y;
    piboxLogger(LOG_TRACE1, "Position: %f x %f\n", x, y);
    gtk_simplelist_press(slist, x, y, GPOINTER_TO_INT(user_data));
    return(TRUE);
}

/*
 *========================================================================
 * Name:   slist_left
 * Prototype:  void slist_left( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Align text to the left in all slist entries.
 *========================================================================
 */
static gboolean
slist_left(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gint offset;
    gtk_simplelist_set_alignment(slist, PANGO_ALIGN_LEFT);
    gtk_widget_queue_draw(slist_entry);
}

/*
 *========================================================================
 * Name:   slist_center
 * Prototype:  void slist_center( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Align text to the center in all slist entries.
 *========================================================================
 */
static gboolean
slist_center(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gint offset;
    gtk_simplelist_set_alignment(slist, PANGO_ALIGN_CENTER);
    gtk_widget_queue_draw(slist_entry);
}

/*
 *========================================================================
 * Name:   slist_right
 * Prototype:  void slist_right( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Align text to the right in all slist entries.
 *========================================================================
 */
static gboolean
slist_right(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gint offset;
    gtk_simplelist_set_alignment(slist, PANGO_ALIGN_RIGHT);
    gtk_widget_queue_draw(slist_entry);
}

/*
 *========================================================================
 * Name:   button_press
 * Prototype:  void button_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a button press (as with a mouse).
 *========================================================================
 */
static gboolean
button_press(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;
    char        *key;
    char        *buf = NULL;
    char        *entry;

    entry = gtk_textline_get(keyboard_entry);
    x = event->x;
    y = event->y;
    piboxLogger(LOG_TRACE1, "Position: %f x %f\n", x, y);
    gtk_keyboard_press(keyboard, x, y, GPOINTER_TO_INT(user_data));
    gtk_widget_queue_draw(widget);
    key = gtk_keyboard_get_key(keyboard);
    if ( key )
    {
        piboxLogger(LOG_TRACE1, "Key: %s\n", key);
        if ( strcasecmp(key, "clear") == 0 )
        {
            buf = (char *)calloc(1, 2);
            sprintf(buf, "");
        }
        else if ( strcasecmp(key, "enter") == 0 )
        {
            piboxLogger(LOG_TRACE1, "Enter Key is ignored in demo.\n");
        }
        else if ( strcasecmp(key, "backspace") == 0 )
        {
            if (strlen(entry) > 0)
            {
                memset((char *)(entry+strlen(entry)-1), 0, 1);
                buf = (char *)calloc(1, strlen(entry) + 2);
                sprintf(buf, "%s", entry );
            }
        }
        else if ( strcasecmp(key, "space") == 0 )
        {
            if (entry)
            {
                buf = (char *)calloc(1, strlen(entry) + 2);
                sprintf(buf, "%s ", entry );
            }
            else
            {
                buf = (char *)calloc(1, 2);
                sprintf(buf, " ");
            }
        }
        else if ( strcasecmp(key, "shift") == 0 )
        {
            gtk_keyboard_shift(keyboard);
        }
        else if ( strcasecmp(key, "page") == 0 )
        {
            gtk_keyboard_increment_page(keyboard);
        }
        else if ( strcasecmp(key, "alpha") == 0 )
        {
            gtk_keyboard_increment_page(keyboard);
        }

        else if ( entry )
        {
            buf = (char *)calloc(1, strlen(entry) + strlen(key) + 1);
            sprintf(buf, "%s%s", entry, key);
        }
        else
        {
            buf = (char *)calloc(1, strlen(key) + 1);
            sprintf(buf, "%s", key);
        }
        free(key);
        if ( buf ) 
        {
            gtk_textline_set(keyboard_entry, buf);
            free(buf);
        }
    }
    else
        piboxLogger(LOG_TRACE1, "Key: Unknown\n");

    if (entry)
        free(entry);
    return(TRUE);
}

/*
 *========================================================================
 * Name:   button_release
 * Prototype:  void button_release( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a button release (as with a mouse).
 *========================================================================
 */
static gboolean
button_release(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gdouble     x, y;
    x = event->x;
    y = event->y;
    piboxLogger(LOG_TRACE1, "Position: %f x %f\n", x, y);
    gtk_keyboard_press(keyboard, x, y, GPOINTER_TO_INT(user_data));
    return(TRUE);
}

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * ========================================================================
 */
static void
sigHandler( int sig )
{
    switch (sig)
    {
        case SIGHUP:
        case SIGTERM:
        case SIGINT:
        case SIGQUIT:
            gtk_main_quit();
            break;
    }
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the window with all possible widgets.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget           *window;
    GtkWidget           *mainvbox;
    GtkWidget           *vbox;
    GtkWidget           *hbox;
    GtkWidget           *notebook;
    GtkWidget           *label;
    GtkWidget           *button;
    gint                row, col, idx;
    guint               appCount;
    guint               numRows;
    guint               numCols;
    guint               statusHeight=0;
    guint               statusWidth=0;
    GdkRectangle        dest;
    int                 width=0;
    int                 height=0;
    GdkScreen           *screen;
    gint                monitor;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );

    screen = gdk_screen_get_default();
    monitor = gdk_screen_get_primary_monitor(screen);
    gdk_screen_get_monitor_geometry (screen, monitor, &dest);
    width = dest.width;
    height = dest.height;

    mainvbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (mainvbox, "vbox");
    gtk_container_add (GTK_CONTAINER (window), mainvbox);
    gtk_widget_show(mainvbox);

    /* Add a notebook with one tab for each widget. */
    notebook = gtk_notebook_new();
    gtk_notebook_set_tab_pos (GTK_NOTEBOOK (notebook), GTK_POS_TOP);
    gtk_box_pack_start (GTK_BOX (mainvbox), notebook, TRUE, TRUE, 0);
    gtk_widget_show(notebook);

    /*
     * ---------------------------------------------------
     * Keyboard and textline widgets.
     * ---------------------------------------------------
     */
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_widget_show(vbox);
    gtk_widget_set_size_request( vbox, 640, 250);

    label = gtk_label_new("keyboard");
    gtk_widget_show(label);
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), vbox, label);

    keyboard_entry = gtk_textline_new();
    gtk_widget_set_size_request( keyboard_entry, 640, 40);
    gtk_box_pack_start (GTK_BOX (vbox), keyboard_entry, FALSE, TRUE, 0);
    gtk_textline_set_fontsize(keyboard_entry, 16);
    gtk_textline_set_maxlength(keyboard_entry, 24);

    keyboard = gtk_keyboard_new("keyboard-us.json");
    gtk_keyboard_set_fontsize(keyboard, 48);
    gtk_widget_add_events(keyboard, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);
    gtk_box_pack_start (GTK_BOX (vbox), keyboard, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(keyboard), "button_press_event", G_CALLBACK(button_press), GINT_TO_POINTER(1));
    g_signal_connect(G_OBJECT(keyboard), "button_release_event", G_CALLBACK(button_release), GINT_TO_POINTER(0));

    /*
     * ---------------------------------------------------
     * Simple paged list widget.
     * ---------------------------------------------------
     */
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_widget_show(vbox);
    gtk_widget_set_size_request( vbox, 640, 250);

    label = gtk_label_new("List");
    gtk_widget_show(label);
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), vbox, label);

    hbox = gtk_hbox_new (FALSE, 0);
    gtk_widget_set_name (hbox, "hbox");
    gtk_widget_show(hbox);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);

    slist_entry = gtk_textline_new();
    gtk_box_pack_start (GTK_BOX (hbox), slist_entry, TRUE, TRUE, 0);
    gtk_textline_set_fontsize(slist_entry, 16);
    gtk_textline_set_maxlength(slist_entry, 36);

    button = gtk_button_new_with_label ("Hidden");
    gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(hidden_toggle), 0);

    button = gtk_button_new_with_label ("Border");
    gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(border_toggle), 0);

    slist = gtk_simplelist_new();
    gtk_box_pack_start (GTK_BOX (vbox), slist, TRUE, TRUE, 0);
    gtk_widget_add_events(slist, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);
    gtk_simplelist_set_fontsize(slist, 20);
    gtk_simplelist_set(slist, cliOptions.slist_data1);
    active_list=cliOptions.slist_data1;
    g_signal_connect(G_OBJECT(slist), "button_press_event", G_CALLBACK(slist_press), GINT_TO_POINTER(1));
    g_signal_connect(G_OBJECT(slist), "button_release_event", G_CALLBACK(slist_release), GINT_TO_POINTER(0));

    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);
    gtk_widget_set_size_request( hbox, 640, 35);
    gtk_widget_show(hbox);

    button = gtk_button_new_with_label ("Back");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(slist_back), 0);

    button = gtk_button_new_with_label ("forward");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(slist_forward), 0);

    button = gtk_button_new_with_label ("switch");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(slist_switch), 0);

    button = gtk_button_new_with_label ("left");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(slist_left), 0);

    button = gtk_button_new_with_label ("center");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(slist_center), 0);

    button = gtk_button_new_with_label ("right");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(slist_right), 0);

    /*
     * ---------------------------------------------------
     * Icontable
     * ---------------------------------------------------
     */
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_widget_show(vbox);
    gtk_widget_set_size_request( vbox, 640, 250);

    label = gtk_label_new("Icontable");
    gtk_widget_show(label);
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), vbox, label);

    icon_entry = gtk_textline_new();
    gtk_widget_set_size_request( icon_entry, 640, 40);
    gtk_box_pack_start (GTK_BOX (vbox), icon_entry, FALSE, TRUE, 0);
    gtk_textline_set_fontsize(icon_entry, 16);
    gtk_textline_set_maxlength(icon_entry, 24);

    icontable = gtk_icontable_new(3, 3);
    gtk_icontable_set(icontable, cliOptions.icons);
    gtk_widget_add_events(icontable, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);
    gtk_box_pack_start (GTK_BOX (vbox), icontable, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(icontable), "button_press_event", G_CALLBACK(icon_press), GINT_TO_POINTER(1));
    g_signal_connect(G_OBJECT(icontable), "button_release_event", G_CALLBACK(icon_release), GINT_TO_POINTER(0));

    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);
    gtk_widget_set_size_request( hbox, 640, 35);
    gtk_widget_show(hbox);

    button = gtk_button_new_with_label ("CB");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(icon_cell), 0);
    g_signal_connect(G_OBJECT(button), "button_release_event", G_CALLBACK(icon_cell_release), GINT_TO_POINTER(0));

    button = gtk_button_new_with_label ("CF");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(icon_cell), GINT_TO_POINTER(1));
    g_signal_connect(G_OBJECT(button), "button_release_event", G_CALLBACK(icon_cell_release), GINT_TO_POINTER(1));

    button = gtk_button_new_with_label ("RD");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(icon_row), 0);
    g_signal_connect(G_OBJECT(button), "button_release_event", G_CALLBACK(icon_row_release), GINT_TO_POINTER(0));

    button = gtk_button_new_with_label ("RU");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(icon_row), GINT_TO_POINTER(1));
    g_signal_connect(G_OBJECT(button), "button_release_event", G_CALLBACK(icon_row_release), GINT_TO_POINTER(1));

    button = gtk_button_new_with_label ("PB");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(icon_page), GINT_TO_POINTER(0));

    button = gtk_button_new_with_label ("PF");
    gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
    gtk_widget_show(button);
    g_signal_connect(G_OBJECT(button), "button_press_event", G_CALLBACK(icon_page), GINT_TO_POINTER(1));

    /*
     * ---------------------------------------------------
     * Make the main window die when destroy is issued.
     * ---------------------------------------------------
     */
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    /* Now position the window and set its title */
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    if ( cliOptions.geometryW != -1 )
        gtk_window_set_default_size(GTK_WINDOW(window), cliOptions.geometryW, cliOptions.geometryH);
    else
        gtk_window_set_default_size(GTK_WINDOW(window), 640, 250);

    gtk_window_set_title(GTK_WINDOW(window), "pigtk");

    return window;
}

/*
 *========================================================================
 * Name:   main
 * Prototype:  int main ( int, char ** )
 *
 * Description:
 * Test all widgets.
 *========================================================================
 */
int
main (int argc, char *argv[])
{
    char    cwd[512];
    char    filePath[PATH_MAX];

    GtkWidget *window;

    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);

    /* Handle signals */
    signal(SIGQUIT, sigHandler);
    signal(SIGTERM, sigHandler);
    signal(SIGHUP, sigHandler);
    signal(SIGINT, sigHandler);

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_TRACE1, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_TRACE1, "No log file configured.\n");
    }
    piboxLogger(LOG_TRACE1, "Size of slist data1: %d.\n", g_slist_length(cliOptions.slist_data1));
    piboxLogger(LOG_TRACE1, "Size of slist data2: %d.\n", g_slist_length(cliOptions.slist_data2));
    piboxLogger(LOG_TRACE1, "Size of icons: %d.\n", g_slist_length(cliOptions.icons));
    
    /* Get the current working directory. */
    memset(cwd, 0, 512);
    getcwd(cwd, 512);

    gtk_init(&argc, &argv);
    if ( isCLIFlagSet( CLI_TEST ) )
    {
        piboxLogger(LOG_TRACE1, "Using gtkrc: %s\n", F_GTKRC);
        gtk_rc_parse(F_GTKRC);
    }

    window = createWindow();
    gtk_widget_show_all(window);
    gtk_keyboard_press(keyboard, -1, -1, 0);

    gtk_main();

    // gdk_threads_leave();

    piboxLoggerShutdown();
    return 0;
}
