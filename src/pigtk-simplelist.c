/*******************************************************************************
 * pigtk-simplelist
 *
 * pigtk-simplelist.h:  custom widget for displaying single line of text.
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 * 
 * Based on:
 * Custom GTK+ widget - http://zetcode.com/tutorials/gtktutorial/customwidget/
 * Clock Widget - https://github.com/humbhenri/clocks/tree/master/gtk-clock
 ******************************************************************************/
#define SIMPLELISTWIDGET_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <glib.h>
#include <pibox/log.h>
#include "pigtk-simplelist.h"

#define FONT_SIZE    12
#define NUM_ROWS     5

/*
 *========================================================================
 * Prototypes
 *========================================================================
 */
static void gtk_simplelist_class_init(GtkSimplelistClass *klass);
static void gtk_simplelist_init(GtkSimplelist *simplelist);
static void gtk_simplelist_size_request(GtkWidget *widget, GtkRequisition *requisition);
static void gtk_simplelist_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void gtk_simplelist_realize(GtkWidget *widget);
static gboolean gtk_simplelist_expose(GtkWidget *widget, GdkEventExpose *event);
static void gtk_simplelist_paint(GtkWidget *widget);
static void gtk_simplelist_destroy(GtkObject *object);

/*
 *========================================================================
 * Name:   do_drawing
 * Prototype:  void do_drawing( GtkWidget *, int, guint, guint )
 *
 * Description:
 * Updates a specific cell of the pixmap in the widget.
 * 
 * Arguments:
 * GtkWidget *      The widget in which we'll do the drawing           
 * int              Row of the list to update or -1 to draw page buttons.
 * int              0: no highlight; 1: draw a highlight beneath the image; 2: clear highlight
 * int              1: paint the background only.
 *========================================================================
 */
static void 
do_drawing( GtkSimplelist *slist, int row, guint doHighlight, guint bg )
{
    GtkSimplelist           *simpleList;
    int                     width, height;
    cairo_t                 *cr_pixmap;
    cairo_surface_t         *cst;
    cairo_t                 *cr;
    guint                   offset_y;
    guint                   offset_x;
    guint                   rowHeight;

    PangoFontDescription    *desc;
    PangoRectangle          pangoRectangle;
    PangoAttrList           *attrs = NULL;
    PangoLayout             *layout;
    char                    buf[256];
    char                    *text;

    /* If requested, just paint the background. */
    if ( bg )
    {
        piboxLogger(LOG_TRACE1, "Paint the background only.\n");

        gdk_drawable_get_size(slist->pixmap, &width, &height);
        cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
        cr = cairo_create(cst);

        cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
        cairo_rectangle(cr, 0, 0, width, height);
        cairo_fill(cr);

        cairo_set_source_rgba(cr, 0, 0, 0, 1.0);
        cairo_move_to(cr, 0, 0);
        cairo_line_to(cr, (double)width, 0);
        cairo_line_to(cr, (double)width, (double)height);
        cairo_line_to(cr, 0, (double)height);
        cairo_line_to(cr, 0, 0);
        cairo_set_line_width(cr, 1);
        cairo_stroke(cr); 

        cr_pixmap = gdk_cairo_create(slist->pixmap);
        cairo_set_source_surface (cr_pixmap, cst, 0, 0);
        cairo_paint(cr_pixmap);
        cairo_destroy(cr_pixmap);

        cairo_surface_destroy(cst);
        cairo_destroy(cr);

        return;
    }

    piboxLogger(LOG_TRACE1, "Looking for row %d out of %d\n", row, g_slist_length(slist->entries));
    if ( ( row < 0 ) || (row > g_slist_length(slist->entries)) )
    {
        piboxLogger(LOG_ERROR, "Invalid row.  Ignoring.\n");
        return;
    }
    gdk_drawable_get_size(slist->pixmap, &width, &height);
    piboxLogger(LOG_TRACE1, "Drawable w/h: %d, %d\n", width, height);
    rowHeight = height / slist->rows;
    offset_y = row * rowHeight;

    piboxLogger(LOG_TRACE2, "Getting cr\n");
    cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, rowHeight);
    cr = cairo_create(cst);

    // Fill with white background
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_rectangle(cr, 0, 0, width, rowHeight);
    cairo_fill(cr);

    /*
     * If requested, draw an outline as a "highlight"
     */
    if ( doHighlight == 0 )
    {
        piboxLogger(LOG_TRACE1, "Drawing normal outline.\n");
        cairo_set_source_rgba(cr, 0, 0, 0, 1.0);
        cairo_move_to(cr, 0, 0);
        cairo_line_to(cr, (double)width, 0);
        cairo_line_to(cr, (double)width, (double)rowHeight);
        cairo_line_to(cr, 0, (double)rowHeight);
        cairo_line_to(cr, 0, 0);
        cairo_set_line_width(cr, 1);
        cairo_stroke(cr); 
    }
    else
    {
        piboxLogger(LOG_TRACE1, "Drawing hightlight outline.\n");
        cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
        cairo_move_to(cr, 0, 0);
        cairo_line_to(cr, (double)width, 0);
        cairo_line_to(cr, (double)width, (double)rowHeight);
        cairo_line_to(cr, 0, (double)rowHeight);
        cairo_line_to(cr, 0, 0);
        cairo_set_line_width(cr, 1);
        cairo_stroke(cr); 
    }

    text = g_slist_nth_data(slist->entries, row+slist->offset);
    if ( text != NULL )
    {
        piboxLogger(LOG_TRACE1, "Text to display: %s\n", text);
        layout = pango_cairo_create_layout(cr);
        sprintf(buf, "Nunito Bold %d", slist->fontSize);
        desc = pango_font_description_from_string( buf );
        pango_font_description_set_absolute_size(desc, slist->fontSize*PANGO_SCALE);
        pango_layout_set_font_description(layout, desc);
        pango_font_description_free(desc);
        pango_layout_set_width(layout, -1); // Disable wrap
        pango_layout_set_spacing(layout, 1);
        pango_layout_set_single_paragraph_mode(layout, FALSE);
        pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);
    
        /* Initialize attributes. */
        attrs = pango_attr_list_new();
        pango_attr_list_insert (attrs, pango_attr_underline_new(PANGO_UNDERLINE_NONE));
        pango_layout_set_attributes (layout, attrs);
    
        piboxLogger(LOG_TRACE1, "text: %s\n", text);
        pango_layout_set_text(layout, text, -1);
    
        pango_layout_get_pixel_size(layout, &pangoRectangle.width, &pangoRectangle.height );
        piboxLogger(LOG_TRACE4, "text w/h: %d / %d\n", pangoRectangle.width, pangoRectangle.height);
        cairo_set_source_rgb(cr, 0, 0, 0);
        pango_cairo_update_layout(cr, layout);
    
        /* Don't need to center horizontally because pango_layout_set_alignment does that for us. */
        piboxLogger(LOG_TRACE1, "**** HO: rowHeight: %d\n", rowHeight);
        piboxLogger(LOG_TRACE1, "**** HO: pangoRectH: %d\n", pangoRectangle.height);
        piboxLogger(LOG_TRACE1, "**** HO: Horizontal offset: %d\n", (rowHeight-pangoRectangle.height)/2);
        // cairo_translate(cr, 0, (rowHeight-pangoRectangle.height)/2);

        if ( slist->alignment == PANGO_ALIGN_RIGHT )
        {
            piboxLogger(LOG_TRACE1, "**** HO: alignment: right\n");
            offset_x = (width - pangoRectangle.width);
        }
        else if ( slist->alignment == PANGO_ALIGN_CENTER )
        {
            piboxLogger(LOG_TRACE1, "**** HO: alignment: center\n");
            offset_x = (width/2 - pangoRectangle.width/2);
        }
        else
        {
            piboxLogger(LOG_TRACE1, "**** HO: alignment: left\n");
            offset_x = 0;
        }
        cairo_translate(cr, offset_x, (rowHeight-pangoRectangle.height)/2);

        pango_cairo_show_layout(cr, layout);
    
        /* Cleanup pango */
        pango_attr_list_unref(attrs);
        g_object_unref(layout);
    }
    else
        piboxLogger(LOG_TRACE1, "No text to display.\n");

    piboxLogger(LOG_TRACE1, "Paint the pixmap.\n");
    cr_pixmap = gdk_cairo_create(slist->pixmap);
    cairo_set_source_surface (cr_pixmap, cst, 0, offset_y);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);

    /* Don't need the cairo object now */
    cairo_surface_destroy(cst);
    cairo_destroy(cr);
}

/*
 *========================================================================
 *========================================================================
 * Private API
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:    gtk_simplelistget_type
 * Prototype:   void gtk_simplelistget_type( void )
 *
 * Description:
 * Returns the GtkType for this widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
GtkType
gtk_simplelist_get_type(void)
{
    static GtkType gtk_simplelist_type = 0;
    if (!gtk_simplelist_type) {
        static const GtkTypeInfo gtk_simplelist_info = {
            "GtkSimplelist",
            sizeof(GtkSimplelist),
            sizeof(GtkSimplelistClass),
            (GtkClassInitFunc) gtk_simplelist_class_init,
            (GtkObjectInitFunc) gtk_simplelist_init,
            NULL,
            NULL,
            (GtkClassInitFunc) NULL
        };
        gtk_simplelist_type = gtk_type_unique(GTK_TYPE_WIDGET, &gtk_simplelist_info);
    }
    return gtk_simplelist_type;
}

/*
 *========================================================================
 * Name:    gtk_simplelist_new
 * Prototype:   void gtk_simplelist_new( void )
 *
 * Description:
 * Create the gtk_simplelist instance.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
GtkWidget * gtk_simplelist_new()
{
    return GTK_WIDGET(gtk_type_new(gtk_simplelist_get_type()));
}

/*
 *========================================================================
 * Name:   gtk_simplelist_class_init
 * Prototype:  void gtk_simplelist_class_init( GtkSimplelistClass * )
 *
 * Description:
 * Initialize the widget class structure fields and setup signals.
 * 
 * Arguments:
 * GtkSimplelistClass *      The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_simplelist_class_init(GtkSimplelistClass *klass)
{
    GtkWidgetClass *widget_class;
    GtkObjectClass *object_class;

    widget_class = (GtkWidgetClass *) klass;
    object_class = (GtkObjectClass *) klass;

    widget_class->realize = gtk_simplelist_realize;
    widget_class->size_request = gtk_simplelist_size_request;
    widget_class->size_allocate = gtk_simplelist_size_allocate;
    widget_class->expose_event = gtk_simplelist_expose;
    object_class->destroy = gtk_simplelist_destroy;
}

/*
 *========================================================================
 * Name:   gtk_simplelist_init
 * Prototype:  void gtk_simplelist_init( GtkSimplelist * )
 *
 * Description:
 * Initialize the widget object and create composite window components.
 * The simplelist widget is not a composite, however.  It's just a giant
 * pixmap we draw in.
 * 
 * Arguments:
 * GtkSimplelist *      The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_simplelist_init(GtkSimplelist *simplelist)
{
    simplelist->pixmap = NULL;
    simplelist->entries = NULL;
    simplelist->fontSize = FONT_SIZE;
    simplelist->rows = NUM_ROWS;
    simplelist->active_row = -1;
    simplelist->offset = 0;
    simplelist->alignment = PANGO_ALIGN_LEFT;
}

/*
 *========================================================================
 * Name:   gtk_simplelist_size_request
 * Prototype:  void gtk_simplelist_size_request( GtkWidget *, GtkRequisition * )
 *
 * Description:
 * Tell GTK+ what size the widget should be, initially.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkRequisition *     The widgets requested size to pass back to caller.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_simplelist_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_SIMPLELIST(widget));
    g_return_if_fail(requisition != NULL);
}

/*
 *========================================================================
 * Name:   gtk_simplelist_size_allocate
 * Prototype:  void gtk_simplelist_size_allocate( GtkWidget *, GtkAllocation * )
 *
 * Description:
 * Handle widget resizes.  Basically this means remove the reference to the
 * pixmap (which should free it), reset the size for the widget, then
 * reallocate and paint in the pixmap at the new size.  This will expand
 * the key cells but not change the size of the font drawn in each cell.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkAllocation *      The size allocated for the widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_simplelist_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
    GtkSimplelist *simplelist; 

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_SIMPLELIST(widget));
    g_return_if_fail(allocation != NULL);

    simplelist = GTK_SIMPLELIST(widget);
    widget->allocation = *allocation;
    piboxLogger(LOG_TRACE1, "widget->allocation.w/h: %d/%d\n", widget->allocation.width, widget->allocation.height);
    if ( simplelist->pixmap != NULL )
    {
        g_object_unref(simplelist->pixmap);
        simplelist->pixmap = NULL;
    }
    if (GTK_WIDGET_REALIZED(widget)) {
        gdk_window_move_resize(
            widget->window,
            allocation->x, allocation->y,
            allocation->width, allocation->height
        );
    }
}

/*
 *========================================================================
 * Name:   gtk_simplelist_realize
 * Prototype:  void gtk_simplelist_realize( GtkWidget * )
 *
 * Description:
 * After creating the window, we set its style and background, and put a
 * pointer to the widget in the user data field of the GdkWindow. This
 * last step allows GTK to dispatch events for this window to the correct
 * widget. 
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_simplelist_realize(GtkWidget *widget)
{
    GdkWindowAttr attributes;
    guint attributes_mask;
    GtkStyle *style;
    GtkAllocation alloc;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_SIMPLELIST(widget));

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    gtk_widget_get_allocation(widget, &alloc);

    piboxLogger(LOG_TRACE1, "alloc.x: %d\n", alloc.x);
    piboxLogger(LOG_TRACE1, "alloc.y: %d\n", alloc.y);
    piboxLogger(LOG_TRACE1, "alloc.w: %d\n", alloc.width);
    piboxLogger(LOG_TRACE1, "alloc.h: %d\n", alloc.height);

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x           = alloc.x;
    attributes.y           = alloc.y;
    attributes.width       = alloc.width;
    attributes.height      = alloc.height;
    attributes.wclass      = GDK_INPUT_OUTPUT;
    attributes.event_mask  = gtk_widget_get_events(widget) | GDK_EXPOSURE_MASK;
    attributes_mask        = GDK_WA_X | GDK_WA_Y;

    widget->window = gdk_window_new(
        gtk_widget_get_parent_window (widget),
        & attributes, attributes_mask
    );

    style = gtk_widget_get_style (widget);
    if (style != NULL) 
    {
        GTK_SIMPLELIST(widget)->bg = style->bg[GTK_STATE_NORMAL];
        GTK_SIMPLELIST(widget)->fg = style->fg[GTK_STATE_NORMAL];
    }
    gtk_simplelist_paint(widget);

    gdk_window_set_user_data(widget->window, widget);
    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
}

/*
 *========================================================================
 * Name:   gtk_simplelist_expose
 * Prototype:  void gtk_simplelist_expose( GtkWidget *, GdkEventExpose * )
 *
 * Description:
 * All drawing is done in an off-screen pixmap.  This function copies
 * that pixmap to the widget window to make the updates visible.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkRequisition *     The widgets requested size to pass back to caller.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static gboolean
gtk_simplelist_expose(GtkWidget *widget, GdkEventExpose *event)
{
    int width, height;

    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_SIMPLELIST(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);

    // Update the the widget.
    gtk_simplelist_paint(widget);

    // Copy the entire pixmap over the widget's drawing area.
    if ( GTK_SIMPLELIST(widget)->pixmap != NULL )
    {
        piboxLogger(LOG_TRACE1, "widget->w/h: %d/%d\n", widget->allocation.width, widget->allocation.height);
        gdk_drawable_get_size(GTK_SIMPLELIST(widget)->pixmap, &width, &height);
        piboxLogger(LOG_TRACE1, "pixmap w/h: %d/%d\n", width, height);

        gdk_draw_drawable(widget->window,
            widget->style->fg_gc[GTK_WIDGET_STATE(widget)], GTK_SIMPLELIST(widget)->pixmap,
            event->area.x, event->area.y,
            event->area.x, event->area.y,
            event->area.width, event->area.height);
    }
    return TRUE;
}

/*
 *========================================================================
 * Name:    gtk_simplelist_paint
 * Prototype:   void gtk_simplelist_paint( GtkWidget *widget )
 *
 * Description:
 * Handle painting (data update) of the widget.
 *========================================================================
 */
static void
gtk_simplelist_paint(GtkWidget *widget)
{
    GtkSimplelist   *slist;
    GdkPixmap       *pixmap;
    guint           i;
    static int      painting = 0;

    /* Avoid multiple calls */
    if ( painting )
        return;

    painting = 1;

    if ( GTK_SIMPLELIST(widget)->pixmap == NULL )
    {   
        GTK_SIMPLELIST(widget)->pixmap = 
            gdk_pixmap_new(widget->window,widget->allocation.width,widget->allocation.height,-1);
    }
    piboxLogger(LOG_TRACE1, "Allocation w/h: %d, %d\n", widget->allocation.width, widget->allocation.height);

    /*
     * Draw each key, one at a time.
     */
    slist = GTK_SIMPLELIST(widget);
    do_drawing(slist, -1, 0, 1);
    for (i=0; i<slist->rows; i++)
    {
        do_drawing(slist, i, 0, 0);
    }

    piboxLogger(LOG_TRACE1, "Done - Paint the pixmap.\n");
    painting = 0;
}

/*
 *========================================================================
 * Name:   gtk_simplelist_destroy
 * Prototype:  void gtk_simplelist_destroy( GtkObject * )
 *
 * Description:
 * Clean up the widget when it is removed.  This is only called,
 * at least for this widget, when the application window is closed.
 * 
 * Arguments:
 * GtkObject *          The object class of the widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_simplelist_destroy(GtkObject *object)
{
    // GtkSimplelist *simplelist;
    GtkSimplelistClass *klass;

    g_return_if_fail(object != NULL);
    g_return_if_fail(GTK_IS_SIMPLELIST(object));

    // simplelist = GTK_SIMPLELIST(object);
    klass = gtk_type_class(gtk_widget_get_type());
    if (GTK_OBJECT_CLASS(klass)->destroy) {
        (* GTK_OBJECT_CLASS(klass)->destroy) (object);
    }
}

/*
 *========================================================================
 *========================================================================
 * Public API
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   gtk_simplelist_set
 * Prototype:  void gtk_simplelist_set( GtkWidget *, GSList * )
 *
 * Description:
 * Set an array of strings to use in the list and the array size (re:
 * number of rows of strings).
 * 
 * Arguments:
 * GtkWidget *widget        The simplelist widget.
 * GSList     *entries      The array of strings that must be NULL terminated.
 *
 * Notes:
 * The list must be a GSList where each element in the list holds a pointer to
 * a string in it's data field.  
 *
 * The GSList is not editable by this widget.
 *========================================================================
 */
void
gtk_simplelist_set( GtkWidget *widget, GSList *entries )
{
    GdkRegion       *region;
    GtkSimplelist   *simplelist;
    guint           i;

    if ( entries == NULL )
        return;
    simplelist = GTK_SIMPLELIST(widget);

    if ( g_slist_length(entries) >0 )
        simplelist->entries = entries;
    piboxLogger(LOG_TRACE1, "Initial number of entries to track: %d\n", g_slist_length(entries) );
}

/*
 *========================================================================
 * Name:   gtk_simplelist_get_row
 * Prototype:  char *gtk_simplelist_get(_row GtkWidget *, guint x, guint y )
 *
 * Description:
 * Identify the row based on x,y position and return the text.
 * 
 * Arguments:
 * GtkWidget *widget        The simplelist widget.
 * guint     x              The X position in the simplelist widget.
 * guint     y              The Y position in the simplelist widget.
 *========================================================================
 */
char *
gtk_simplelist_get_row( GtkWidget *widget, guint x, guint y )
{
    GtkSimplelist     *slist;
    gint              row = -1;
    gint              cellHeight;

    slist = GTK_SIMPLELIST(widget);
    cellHeight = widget->allocation.height / slist->rows;
    row = (int)(y / cellHeight) + slist->offset;

    // Convert x/y to a row: TBD
    return g_slist_nth_data(slist->entries, row);
}

/*
 *========================================================================
 * Name:   gtk_simplelist_set_fontsize
 * Prototype:  void gtk_simplelist_set_fontsize( GtkWidget *, guint )
 *
 * Description:
 * Set the size of the font text.  Should be called before the widget is
 * realized.
 * 
 * Arguments:
 * GtkWidget *widget        The simplelist widget.
 *
 * Notes:
 * This should be handled with the gtkrc, but this was the simplest way
 * to implement it for now.
 *========================================================================
 */
void
gtk_simplelist_set_fontsize( GtkWidget *widget, guint size )
{
    GdkRegion         *region;
    GtkSimplelist     *simplelist;

    simplelist = GTK_SIMPLELIST(widget);
    simplelist->fontSize = size;
}

/*
 *========================================================================
 * Name:   gtk_simplelist_set_rows
 * Prototype:  void gtk_simplelist_set_rows( GtkWidget *, guint )
 *
 * Description:
 * Set the number of rows to display.  This is unrelated to the number of
 * rows in the array of strings to be displayed.
 * Should be called before the widget is realized.
 * 
 * Arguments:
 * GtkWidget *widget        The simplelist widget.
 *========================================================================
 */
void
gtk_simplelist_set_rows( GtkWidget *widget, guint rows )
{
    GtkSimplelist     *simplelist;

    simplelist = GTK_SIMPLELIST(widget);
    simplelist->rows = rows;
}

/*
 *========================================================================
 * Name:   gtk_simplelist_get_rows
 * Prototype:  void gtk_simplelist_get_rows( GtkWidget * )
 *
 * Description:
 * Get the number of rows to display.  This is unrelated to the number of
 * rows in the array of strings to be displayed.
 * 
 * Arguments:
 * GtkWidget *widget        The simplelist widget.
 *========================================================================
 */
guint
gtk_simplelist_get_rows( GtkWidget *widget )
{
    GtkSimplelist     *simplelist;

    simplelist = GTK_SIMPLELIST(widget);
    return(simplelist->rows);
}

/*
 *========================================================================
 * Name:   gtk_simplelist_get_offet
 * Prototype:  void gtk_simplelist_get_offet( GtkWidget * )
 *
 * Description:
 * Current offset into list.
 * 
 * Arguments:
 * GtkWidget *widget        The simplelist widget.
 *========================================================================
 */
guint
gtk_simplelist_get_offset( GtkWidget *widget )
{
    GtkSimplelist     *simplelist;

    simplelist = GTK_SIMPLELIST(widget);
    return( simplelist->offset );
}

/*
 *========================================================================
 * Name:   gtk_simplelist_set_alignment
 * Prototype:  void gtk_simplelist_set_alignment( GtkWidget *, PangoAlignment adj )
 *
 * Description:
 * Current offset into list.
 * 
 * Arguments:
 * GtkWidget *widget        The simplelist widget.
 *
 * Notes:
 * adj can be one of the following.
 * SIMPLELIST_LEFT     same as PANGO_ALIGN_LEFT
 * SIMPLELIST_RIGHT    same as PANGO_ALIGN_RIGHT
 * SIMPLELIST_CENTER   same as PANGO_ALIGN_CENTER
 *========================================================================
 */
void
gtk_simplelist_set_alignment( GtkWidget *widget, PangoAlignment adj )
{
    GtkSimplelist     *simplelist;
    GdkRegion         *region;

    simplelist = GTK_SIMPLELIST(widget);
    simplelist->alignment = adj;

    gtk_simplelist_paint(widget);
    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_simplelist_set_offset
 * Prototype:  void gtk_simplelist_set_offset( GtkWidget * )
 *
 * Description:
 * Set the offset into the list where the display starts.
 * 
 * Arguments:
 * GtkWidget *widget        The simplelist widget.
 *========================================================================
 */
void
gtk_simplelist_set_offset( GtkWidget *widget, guint offset )
{
    GtkSimplelist     *simplelist;
    GdkRegion         *region;

    simplelist = GTK_SIMPLELIST(widget);
    simplelist->offset = offset;

    gtk_simplelist_paint(widget);
    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_simplelist_press
 * Prototype:  void gtk_simplelist_press( GtkWidget *, int, int, int )
 *
 * Description:
 * Called with the simplelist window relative position of the line that is to
 * be pressed and released.  This is expected to be by touchscreen but
 * can also be done by mouse clicks.
 * 
 * Arguments:
 * GtkWidget *widget        The simplelist widget.
 * int       x              The x (col) position, relative to the simplelist window.
 * int       y              The y (row) position, relative to the simplelist window.
 * int       highlight      Whether to highlight the key border or not.
 *========================================================================
 */
void
gtk_simplelist_press( GtkWidget *widget, int x, int y, int highlight )
{
    GtkSimplelist   *slist;
    GdkRegion       *region;
    int             i;
    int             row, col;
    int             cellHeight;

    slist = GTK_SIMPLELIST ( widget );

    /* If no index provided, do nothing. */
    if ( (x == -1) && (y == -1) )
    {
        return;
    }
    else
    {
        /* Convert widget coordinates to row */
        cellHeight = widget->allocation.height / slist->rows;
        row = (int)(y / cellHeight);

        /* Update the specific row that's been pressed. */
        if ( row < slist->rows )
        {
            // This should be scheduled with a mutex on drawing.
            do_drawing(slist, row, highlight, highlight);
            slist->active_row = row;
        }    
        else
            piboxLogger(LOG_ERROR, "Invalid simplelist position: %d\n", row);
    }

    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}
