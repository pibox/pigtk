/*******************************************************************************
 * pigtk widget library
 *
 * pigtk-keyboard.c:  Keyboard widget (aka gtk-keyboard)
 *
 * This widget requires libpibox for reading JSON data.
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 * 
 * Based on:
 * Custom GTK+ widget - http://zetcode.com/tutorials/gtktutorial/customwidget/
 * Clock Widget - https://github.com/humbhenri/clocks/tree/master/gtk-clock
 ******************************************************************************/
#define PIGTK_KEYBOARD_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <glib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include <pibox/log.h>
#include <pibox/parson.h>
#include <pibox/pibox.h>
#include "pigtk-keyboard.h"

#define FONT_SIZE       32
#define SHIFT           "shift"
#define LABEL           "label"
#define ICON            "icon"

/* A big static buffer for building multiline text strings. */
#define STR_SIZE    2048
static char str[STR_SIZE];
static char *keyboardFile;

enum {
  KEYBOARD_SIGNAL,
  LAST_SIGNAL
};

static gint keyboard_signals[LAST_SIGNAL] = { 0 };

/*
 *========================================================================
 * Prototypes
 *========================================================================
 */
static void gtk_keyboard_class_init(GtkKeyboardClass *klass);
static void gtk_keyboard_init(GtkKeyboard *keyboard);
static void gtk_keyboard_size_request(GtkWidget *widget, GtkRequisition *requisition);
static void gtk_keyboard_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void gtk_keyboard_realize(GtkWidget *widget);
static gboolean gtk_keyboard_expose(GtkWidget *widget, GdkEventExpose *event);
static void gtk_keyboard_paint(GtkWidget *widget);
static void gtk_keyboard_destroy(GtkObject *object);

/*
 *========================================================================
 *========================================================================
 * Private Functions
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   loadJSON
 * Prototype:  void loadJSON( GtkKeyboard *, char * )
 *
 * Description:
 * Read the keyboard configuration file and save a reference to it
 * in the keyboard object structure.
 * 
 * Arguments:
 * GtkKeyboard *kbd         The main widget structure.
 * char        *keyboarFile The keyboard configuration file, in JSON format.
 *
 * Notes:
 * The caller has already verified the file exists.
 *========================================================================
 */
static void
loadJSON(GtkKeyboard *kbd, char *keyboardFile)
{
    char *str;

    piboxLogger(LOG_TRACE1, "Reading file: %s\n", keyboardFile);
    kbd->root_value = json_parse_file( keyboardFile );
    kbd->root_object = json_value_get_object( kbd->root_value );
    str = json_serialize_to_string_pretty(kbd->root_value);
    piboxLogger(LOG_TRACE1, "JSON:\n%s\n",str);
    json_free_serialized_string(str);
}

/*
 *========================================================================
 * Name:   getCell
 * Prototype:  void getCell( GtkKeyboard *, int )
 *
 * Description:
 * Retrieve the configuration for the key at the specified index.
 * 
 * Arguments:
 * GtkKeyboard *kbd The main widget structure.
 * int index        The index (row, col) to the cell configuration to retrieve.
 *
 * Returns:
 * The JSON_Object at the specified index or NULL if the index doesn't exist.
 *========================================================================
 */
static JSON_Object *
getCell(GtkKeyboard *kbd, int idx)
{
    JSON_Array  *array;

    piboxLogger(LOG_TRACE1, "Retrieving cell %d.\n", idx);
    array = json_object_get_array( kbd->root_object, "keys" );
    if ( array == NULL )
        return NULL;

    return ( json_array_get_object(array, idx) );
}

/*
 *========================================================================
 * Name:   getLabel
 * Prototype:  char *getLabel(GtkKeyboard *, int)
 *
 * Description:
 * Generate the label string to use.
 *
 * Arguments:
 * int type     0 for Label strings, 1 for Icon strings.
 * 
 * Returns:
 * Character string of the field to use, as in "shiftLabel2" or "icon2".
 *
 * Notes:
 * Caller must free allocated memory.
 *========================================================================
 */
static char *
getLabel(GtkKeyboard *kbd, int type)
{
    char    buf[256];
    char    *ptr;
    char    *field;

    memset(buf, 0, 256);
    ptr = buf;

    if ( type )
        field = ICON;
    else
        field = LABEL;

    if ((kbd->shift) && (type == 0) )
        sprintf(ptr, "%s%s", SHIFT, field);
    else
        sprintf(ptr, "%s", field);
    sprintf((char *)(ptr + strlen(buf)), "%d", kbd->activePage);
    piboxLogger(LOG_TRACE1, "JSON id: %s\n", buf);
    return strdup(buf);
}

/*
 *========================================================================
 * Name:   do_drawing
 * Prototype:  void do_drawing( GtkWidget *, int, int, int )
 *
 * Description:
 * Updates a specific cell of the pixmap in the keyboard widget.
 * 
 * Arguments:
 * GtkWidget *      The widget in which we'll do the drawing           
 * int              Row of the button to update.
 * int              Column of the button to update.
 * int              0: no highlight; 1: draw a highlight beneath the image; 2: clear highlight
 *========================================================================
 */
static void 
do_drawing( GtkKeyboard *kbd, int row, int col, int doHighlight)
{
    GdkPixmap       *pixmap;
    cairo_surface_t *cst;
    cairo_t         *cr = NULL;
    cairo_t         *cr_pixmap;
    gdouble         offset_x, offset_y;
    gint            width, height;
    gint            realCellWidth;
    gint            realCellHeight;
    GdkPixbuf       *image = NULL;
    GdkPixbuf       *newimage;
    JSON_Object     *cell;
    const char      *label;
    const char      *keysym;
    const char      *icon;
    const char      *color;
    char            *filename;
    char            *labelStr;;
    char            *iconStr;;
    cairo_text_extents_t    extents;

    /* Retrieve configuration for this cell. */
    cell = getCell(kbd, (row*kbd->cols)+col);
    if ( cell == NULL )
    {
        piboxLogger(LOG_TRACE1, "No such key at index %d\n", (int)(row*kbd->cols)+col);
        return;
    }

    /* Choose the correct page id. */
    labelStr = getLabel(kbd,0);
    piboxLogger(LOG_TRACE1, "JSON label id: %s\n", labelStr);
    label = json_object_get_string(cell, labelStr);
    free(labelStr);

    iconStr = getLabel(kbd,1);
    piboxLogger(LOG_TRACE1, "JSON icon id: %s\n", iconStr);
    icon = json_object_get_string(cell, iconStr);

    /* Color is not yet supported. */
    color = json_object_get_string(cell, "color");

    if ( kbd->pixmap == NULL )
    {
        piboxLogger(LOG_TRACE1, "No pixmap to draw in.\n");
        return;
    }
    pixmap = kbd->pixmap;

    // Compute key position relative to kbd->pixmap;
    offset_x = kbd->cellWidth * col;
    offset_y = kbd->cellHeight * row;

    // create a gtk-independant surface to draw on
    if ( col == (kbd->cols - 1) && kbd->col_pad > 0 )
    {
        // Last column can be padded up to a full cell width - 1.
        realCellWidth = kbd->cellWidth+kbd->col_pad;
    }
    else
    {
        realCellWidth = kbd->cellWidth;
    }
    if ( row == (kbd->rows - 1) && kbd->row_pad > 0 )
    {
        // Last row can be padded up to a full cell height - 1.
        realCellHeight = kbd->cellHeight+kbd->row_pad;
    }
    else
    {
        realCellHeight = kbd->cellHeight;
    }
    cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, realCellWidth, realCellHeight);
    cr = cairo_create(cst);

    /* Default to white background */
    piboxLogger(LOG_TRACE1, "Setting cell background to white.\n");
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
    cairo_rectangle(cr, 0, 0, (double)realCellWidth, (double)realCellHeight);
    cairo_fill(cr);
    cairo_paint(cr);

    /* Load the button icon if configured. */
    if ( icon )
    {
        filename = (char *)calloc(1, strlen(kbd->keyboardDir) + strlen(icon) + 2);
        sprintf(filename, "%s/%s", kbd->keyboardDir, icon);
        piboxLogger(LOG_TRACE1, "Setting cell background to an icon: %s\n", filename);
        image = gdk_pixbuf_new_from_file(filename, NULL);
        width  = gdk_pixbuf_get_width(image);
        height = gdk_pixbuf_get_height(image);
        piboxLogger(LOG_TRACE1, "Icon image: w/h: %d / %d \n", width, height);
        newimage = gdk_pixbuf_scale_simple(image, realCellWidth, realCellHeight, GDK_INTERP_BILINEAR);
        g_object_unref(image);
        free(filename);
        image = newimage;

        /* Positioning */
        gdk_cairo_set_source_pixbuf(cr, image, 0, 0);
        cairo_paint(cr);    
        g_object_unref(image);
    }
    else if ( ( label ) && ( strcasecmp(label, "nokey") != 0 ) )
    {
        /* Draw the text over the background. */
        piboxLogger(LOG_TRACE1, "Drawing text over background.\n");
        cairo_set_source_rgb(cr, 0.3, 0.3, 0.3);
        cairo_select_font_face(cr, "Courier",
            CAIRO_FONT_SLANT_NORMAL,
            CAIRO_FONT_WEIGHT_BOLD);

        cairo_set_font_size(cr, kbd->fontSize);
        cairo_text_extents(cr, label, &extents);
        cairo_move_to(cr, realCellWidth/2 - extents.width/2, realCellHeight/2 + extents.height/2);
        cairo_show_text(cr, label);
    }

    /*
     * If requested, draw an outline as a "highlight"
     */
    if ( doHighlight == 0 )
    {
        piboxLogger(LOG_TRACE1, "Drawing normal outline.\n");
        cairo_set_source_rgba(cr, 0, 0, 0, 1.0);
        cairo_move_to(cr, 0, 0);
        cairo_line_to(cr, (double)realCellWidth, 0);
        cairo_line_to(cr, (double)realCellWidth, (double)realCellHeight);
        cairo_line_to(cr, 0, (double)realCellHeight);
        cairo_line_to(cr, 0, 0);
        cairo_set_line_width(cr, 1);
        cairo_stroke(cr); 
    }
    else
    {
        piboxLogger(LOG_TRACE1, "Drawing hightlight outline.\n");
        cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
        cairo_move_to(cr, 0, 0);
        cairo_line_to(cr, (double)realCellWidth, 0);
        cairo_line_to(cr, (double)realCellWidth, (double)realCellHeight);
        cairo_line_to(cr, 0, (double)realCellHeight);
        cairo_line_to(cr, 0, 0);
        cairo_set_line_width(cr, 1);
        cairo_stroke(cr); 
    }
    // cairo_paint(cr); 

    cairo_destroy(cr);
    cr = NULL;

    cr_pixmap = gdk_cairo_create(pixmap);
    cairo_set_source_surface (cr_pixmap, cst, offset_x, offset_y);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);
    cairo_surface_destroy(cst);
}

/*
 *========================================================================
 *========================================================================
 * Private API
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:    gtk_keyboard_get_type
 * Prototype:   void gtk_keyboard_get_type( void )
 *
 * Description:
 * Returns the GtkType for this widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
GtkType
gtk_keyboard_get_type(void)
{
    static GtkType gtk_keyboard_type = 0;
    if (!gtk_keyboard_type) {
        static const GtkTypeInfo gtk_keyboard_info = {
            "GtkKeyboard",
            sizeof(GtkKeyboard),
            sizeof(GtkKeyboardClass),
            (GtkClassInitFunc) gtk_keyboard_class_init,
            (GtkObjectInitFunc) gtk_keyboard_init,
            NULL,
            NULL,
            (GtkClassInitFunc) NULL
        };
        gtk_keyboard_type = gtk_type_unique(GTK_TYPE_WIDGET, &gtk_keyboard_info);
    }
    return gtk_keyboard_type;
}

/*
 *========================================================================
 * Name:    gtk_keyboard_new
 * Prototype:   void gtk_keyboard_new( char *keyboardFile )
 *
 * Description:
 * Create the gtk_keyboard instance.  Loads the keyboard file if 
 * found.  
 *
 * Arguments:
 * char *keyboarFile    Name of keyboard configuration file to use.
 *
 * Notes:
 * keyboard_file must exist under /usr/share/gtkkeyboard/.
 * Standard GTK widget API function.
 *========================================================================
 */
GtkWidget *gtk_keyboard_new(char *file)
{
    GtkKeyboard *kbd;

    piboxLogger(LOG_TRACE1, "file = %s.\n", file);
    keyboardFile=strdup(file);
    return GTK_WIDGET( gtk_type_new(gtk_keyboard_get_type()) );
}

/*
 *========================================================================
 * Name:   gtk_keyboard_class_init
 * Prototype:  void gtk_keyboard_class_init( GtkKeyboardClass * )
 *
 * Description:
 * Initialize the widget class structure fields and setup signals.
 * 
 * Arguments:
 * GtkKeyboardClass *      The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_keyboard_class_init(GtkKeyboardClass *klass)
{
    GtkWidgetClass *widget_class;
    GtkObjectClass *object_class;

    widget_class = (GtkWidgetClass *) klass;
    object_class = (GtkObjectClass *) klass;

    widget_class->realize       = gtk_keyboard_realize;
    widget_class->size_request  = gtk_keyboard_size_request;
    widget_class->size_allocate = gtk_keyboard_size_allocate;
    widget_class->expose_event  = gtk_keyboard_expose;
    object_class->destroy       = gtk_keyboard_destroy;
}

/*
 *========================================================================
 * Name:   gtk_keyboard_init
 * Prototype:  void gtk_keyboard_init( GtkKeyboard * )
 *
 * Description:
 * Initialize the widget object and create composite window components.
 * The keyboard widget is not a composite, however.  It's just a giant
 * pixmap we draw in.
 * 
 * Arguments:
 * GtkKeyboard *      The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_keyboard_init(GtkKeyboard *kbd)
{
    int             numRows, numCols;
    int             row, col;
    GtkWidget       *dwga;
    GtkWidget       *button;
    GtkObjectClass  *object_class;
    int             idx;
    char            *buf;
    struct stat     stat_buf;

    if ( keyboardFile == NULL )
    {
        piboxLogger(LOG_ERROR, "Missing keyboard file.\n");
        return;
    }
    kbd->keyboardFile = keyboardFile;
    kbd->shift = 0;
    kbd->fontSize = FONT_SIZE;
    kbd->activePage = 0;

    if ( getenv("GTK_KEYBOARD_DIR") != NULL )
        kbd->keyboardDir = strdup( getenv("GTK_KEYBOARD_DIR") );
    else
        kbd->keyboardDir = strdup(GTK_KEYBOARD_DIR);

    /* Build full pathname */
    buf = (char *)calloc(1, strlen(kbd->keyboardDir) + strlen(kbd->keyboardFile) + 2);
    sprintf(buf, "%s/%s", kbd->keyboardDir, kbd->keyboardFile);

    /* Test for specified keyboard configuration file. */
    if ( stat(buf, &stat_buf) == 0 )
    {
        /* Load keyboard configuration. */
        loadJSON(kbd, buf);
    }
    else
    {
        /* Can't create the keyboard without a valid configuration. */
        piboxLogger(LOG_ERROR, "Missing keyboard configuration file: %s\n", buf);
        kbd->root_value  = NULL;
        kbd->root_object = NULL;
        if ( kbd->keyboardFile )
        {
            free(kbd->keyboardFile);
            kbd->keyboardFile = NULL;
        }
        return;
    }

    kbd->rows = numRows = (int)json_object_get_number(kbd->root_object, "rows");
    kbd->cols = numCols = (int)json_object_get_number(kbd->root_object, "columns");
    if ( (numRows == 0) || (numCols == 0) )
    {
        piboxLogger(LOG_ERROR, "Invalid rows (%d) or columns (%d) in GTK_KEYBOARD.\n", numRows, numCols);
        if ( kbd->keyboardFile )
        {
            free(kbd->keyboardFile);
            kbd->keyboardFile = NULL;
        }
        return;
    }
    piboxLogger(LOG_TRACE1, "Rows (%d) and columns (%d) in keyboard config.\n", numRows, numCols);

    kbd->pages = (int)json_object_get_number(kbd->root_object, "pages");
    piboxLogger(LOG_TRACE1, "Page in keyboard config: %d\n", kbd->pages);

    object_class = (GtkObjectClass *)kbd;
}

/*
 *========================================================================
 * Name:   gtk_keyboard_size_request
 * Prototype:  void gtk_keyboard_size_request( GtkWidget *, GtkRequisition * )
 *
 * Description:
 * Tell GTK+ what size the widget should be, initially.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkRequisition *     The widgets requested size to pass back to caller.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_keyboard_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
    GdkScreen       *screen;
    gint            monitor;
    GdkRectangle    dest;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_KEYBOARD(widget));
    g_return_if_fail(requisition != NULL);
}

/*
 *========================================================================
 * Name:   gtk_keyboard_size_allocate
 * Prototype:  void gtk_keyboard_size_allocate( GtkWidget *, GtkAllocation * )
 *
 * Description:
 * Handle widget resizes.  Basically this means remove the reference to the
 * pixmap (which should free it), reset the size for the widget, then
 * reallocate and paint in the pixmap at the new size.  This will expand
 * the key cells but not change the size of the font drawn in each cell.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkAllocation *      The size allocated for the widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_keyboard_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
    GtkKeyboard *kbd;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_KEYBOARD(widget));
    g_return_if_fail(allocation != NULL);

    widget->allocation = *allocation;
    piboxLogger(LOG_TRACE1, "Allocated w,h: %d,%d\n", allocation->width, allocation->height);
    piboxLogger(LOG_TRACE1, "Allocated x,y: %d,%d\n", allocation->x, allocation->y);
    kbd = GTK_KEYBOARD(widget);
    kbd->width = allocation->width;
    kbd->height = allocation->height;
    kbd->cellWidth = allocation->width / kbd->cols;
    kbd->cellHeight = allocation->height / kbd->rows;
    kbd->col_pad = allocation->width % kbd->cols;
    kbd->row_pad = allocation->height % kbd->rows;
    piboxLogger(LOG_TRACE1, "Cell x,y,cpad,rpad: %d,%d,%d,%d\n", 
            kbd->cellWidth, kbd->cellHeight, kbd->col_pad, kbd->row_pad);
    if ( kbd->pixmap != NULL )
    {
        g_object_unref(kbd->pixmap);
        kbd->pixmap = NULL;
    }
    if (GTK_WIDGET_REALIZED(widget)) {
        gdk_window_move_resize(
            widget->window,
            allocation->x, allocation->y,
            allocation->width, allocation->height
        );
    }
    gtk_keyboard_paint(widget);
}

/*
 *========================================================================
 * Name:   gtk_keyboard_realize
 * Prototype:  void gtk_keyboard_realize( GtkWidget * )
 *
 * Description:
 * After creating the window, we set its style and background, and put a
 * pointer to the widget in the user data field of the GdkWindow. This
 * last step allows GTK to dispatch events for this window to the correct
 * widget. 
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_keyboard_realize(GtkWidget *widget)
{
    GtkKeyboard *kbd;
    GdkWindowAttr attributes;
    guint attributes_mask;
    GtkStyle *style;
    GtkAllocation alloc;
    int row, col;
    int idx;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_KEYBOARD(widget));

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    gtk_widget_get_allocation(widget, &alloc);

    piboxLogger(LOG_TRACE1, "alloc.x: %d\n", alloc.x);
    piboxLogger(LOG_TRACE1, "alloc.y: %d\n", alloc.y);
    piboxLogger(LOG_TRACE1, "alloc.w: %d\n", alloc.width);
    piboxLogger(LOG_TRACE1, "alloc.h: %d\n", alloc.height);

    kbd = GTK_KEYBOARD(widget);
    kbd->width = alloc.width;
    kbd->height = alloc.height;
    kbd->cellWidth = alloc.width / kbd->cols;
    kbd->cellHeight = alloc.height / kbd->rows;
    piboxLogger(LOG_TRACE1, "cellWidth: %d\n", kbd->cellWidth);
    piboxLogger(LOG_TRACE1, "cellHeight: %d\n", kbd->cellHeight);

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x           = alloc.x;
    attributes.y           = alloc.y;
    attributes.width       = alloc.width;
    attributes.height      = alloc.height;
    attributes.wclass      = GDK_INPUT_OUTPUT;
    attributes.event_mask  = gtk_widget_get_events(widget) | GDK_EXPOSURE_MASK;
    attributes_mask        = GDK_WA_X | GDK_WA_Y;

    widget->window = gdk_window_new(
        gtk_widget_get_parent_window (widget),
        & attributes, attributes_mask
    );

    style = gtk_widget_get_style (widget);
    if (style != NULL) 
    {
        GTK_KEYBOARD(widget)->bg = style->bg[GTK_STATE_NORMAL];
        GTK_KEYBOARD(widget)->fg = style->fg[GTK_STATE_NORMAL];
    }

    gdk_window_set_user_data(widget->window, widget);
    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
}

/*
 *========================================================================
 * Name:   gtk_keyboard_expose
 * Prototype:  void gtk_keyboard_expose( GtkWidget *, GdkEventExpose * )
 *
 * Description:
 * All drawing is done in an off-screen pixmap.  This function copies
 * that pixmap to the widget window to make the updates visible.
 * 
 * Arguments:
 * GtkWidget *          The widget to initialize.
 * GtkRequisition *     The widgets requested size to pass back to caller.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static gboolean
gtk_keyboard_expose(GtkWidget *widget, GdkEventExpose *event)
{
    int width, height;

    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_KEYBOARD(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);

    // Copy the entire pixmap over the widget's drawing area.
    if ( GTK_KEYBOARD(widget)->pixmap != NULL )
    {
        piboxLogger(LOG_TRACE1, "widget->w/h: %d/%d\n", widget->allocation.width, widget->allocation.height);
        gdk_drawable_get_size(GTK_KEYBOARD(widget)->pixmap, &width, &height);
        piboxLogger(LOG_TRACE1, "pixmap w/h: %d/%d\n", width, height);

        gdk_draw_drawable(widget->window,
            widget->style->fg_gc[GTK_WIDGET_STATE(widget)], GTK_KEYBOARD(widget)->pixmap,
            event->area.x, event->area.y,
            event->area.x, event->area.y,
            event->area.width, event->area.height);
    }
    return TRUE;
}

/*
 *========================================================================
 * Name:    gtk_keyboard_paint
 * Prototype:   void gtk_keyboard_paint( GtkWidget *widget )
 *
 * Description:
 * Handle painting (data update) of the widget.
 * This is the front end to the do_drawing() function that iterates the
 * complete set of cells in the keyboard.
 *
 * Arguments:
 * GtkWidget *widget        The widget to paint.
 *
 * Notes:
 * This is NOT a standard GTK widget API function.
 *========================================================================
 */
static void
gtk_keyboard_paint(GtkWidget *widget)
{
    GtkKeyboard             *kbd;
    int                     i,j;
    static int              painting = 0;

    /* Avoid multiple calls */
    if ( painting )
        return;
    painting = 1;

    if ( GTK_KEYBOARD(widget)->pixmap == NULL )
    {   
        piboxLogger(LOG_TRACE1, "Allocating pixmap\n");
        GTK_KEYBOARD(widget)->pixmap =
            gdk_pixmap_new(widget->window,widget->allocation.width,widget->allocation.height,-1);
    }

    /*
     * Draw each key, one at a time.
     */
    kbd = GTK_KEYBOARD( widget );
    for (i=0; i<kbd->rows; i++)
    {
        for (j=0; j<kbd->cols; j++)
        {
            do_drawing(kbd, i, j, 0);
        }
    }
    painting = 0;
}

/*
 *========================================================================
 * Name:   gtk_keyboard_destroy
 * Prototype:  void gtk_keyboard_destroy( GtkObject * )
 *
 * Description:
 * Clean up the widget when it is removed.  This is only called,
 * at least for this widget, when the application window is closed.
 * 
 * Arguments:
 * GtkObject *          The object class of the widget.
 *
 * Notes:
 * Standard GTK widget API function.
 *========================================================================
 */
static void
gtk_keyboard_destroy(GtkObject *object)
{
    GtkKeyboard *kbd;
    GtkKeyboardClass *klass;

    g_return_if_fail(object != NULL);
    g_return_if_fail(GTK_IS_KEYBOARD(object));

    kbd = GTK_KEYBOARD(object);

    /* 
     * Clean up JSON storage - this causes segfaults
     * when called from here for some reason.  So cleanup
     * of JSON memory is done by application shutdown
     * instead.
     */
    // if ( kbd->root_value )
        // json_value_free(kbd->root_value);

    /* Free keyboard config filename */
    if (kbd->keyboardFile) 
    {
        free(kbd->keyboardFile);
        kbd->keyboardFile = NULL;
    }

    klass = gtk_type_class(gtk_widget_get_type());
    if (GTK_OBJECT_CLASS(klass)->destroy) {
        (* GTK_OBJECT_CLASS(klass)->destroy) (object);
    }
}

/*
 *========================================================================
 *========================================================================
 * Public API
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   gtk_keyboard_press
 * Prototype:  void gtk_keyboard_press( GtkWidget *, int, int, int )
 *
 * Description:
 * Called with the keyboard window relative position of the key that is to
 * be pressed and release.  This is expected to be by touchscreen but
 * can also be done by mouse clicks.
 * 
 * Arguments:
 * GtkWidget *widget        The keyboard widget.
 * int       x              The x (col) position, relative to the keyboard window.
 * int       y              The y (row) position, relative to the keyboard window.
 * int       highlight      Whether to highlight the key border or not.
 *========================================================================
 */
void
gtk_keyboard_press( GtkWidget *widget, int x, int y, int highlight )
{
    GtkKeyboard     *kbd;
    GdkRegion       *region;
    int             i;
    int             row, col;

    kbd = GTK_KEYBOARD ( widget );

    /* If no index provided, redraw all the keys.  Common for first time map/expose events. */
    if ( (x == -1) && (y == -1) )
    {
        gtk_keyboard_paint(widget);
    }
    else
    {
        /* Convert widget coordinates to row/col */
        row = (int)(y / kbd->cellHeight);
        col = (int)(x / kbd->cellWidth);

        /* Update the specific key that's been pressed. */
        if ( (row < kbd->rows) && (col < kbd->cols) )
        {
            // This should be scheduled with a mutex on drawing.
            do_drawing(kbd, row, col, highlight);
            kbd->active_cell.row = row;
            kbd->active_cell.col = col;
        }    
        else
            piboxLogger(LOG_ERROR, "Invalid keyboard position: %d,%d\n", row, col);
    }

    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_keyboard_get_key
 * Prototype:  void gtk_keyboard_get_key( GtkWidget * )
 *
 * Description:
 * Retrieve the current active key, which was set by the last 
 * call to gtk_keyboard_press().  The key is retrieved based
 * on the row/col index.
 * 
 * Arguments:
 * GtkWidget *          The keyboard widget.
 *
 * Returns:
 * The string associated with the key.  The caller is responsible
 * for freeing the returned buffer.  If the active cell does not
 * have a corresponding "label" or "shiftlabel" (as appropriate)
 * then NULL is returned.
 *========================================================================
 */
char *
gtk_keyboard_get_key( GtkWidget *widget )
{
    GtkKeyboard     *kbd = GTK_KEYBOARD(widget);
    int             row, col;
    const char      *label;
    char            *labelStr;
    JSON_Object     *cell;

    row = kbd->active_cell.row;
    col = kbd->active_cell.col;
    cell = getCell(kbd, (row*kbd->cols)+col);
    if ( cell == NULL )
    {
        piboxLogger(LOG_TRACE1, "No such key at index %d\n", (int)(row*kbd->cols)+col);
        return NULL;
    }

    labelStr = getLabel(kbd,0);
    piboxLogger(LOG_TRACE1, "JSON id: %s\n", labelStr);
    label = json_object_get_string(cell, labelStr);
    free(labelStr);

    if ( (label != NULL) && (strcasecmp(label,"nokey") != 0) )
        return strdup(label);
    else
        return NULL;
}

/*
 *========================================================================
 * Name:   gtk_keyboard_shift
 * Prototype:  void gtk_keyboard_shift( GtkWidget * )
 *
 * Description:
 * Toggle use of "shiftlabel".  When enabled, the "shiftlabel" field
 * is returned in gtk_keyboard_get_key().  When not enabled the "label"
 * field is returned instead.
 *
 * Arguments:
 * GtkWidget *          The keyboard widget.
 *========================================================================
 */
void
gtk_keyboard_shift( GtkWidget *widget )
{
    GtkKeyboard     *kbd = GTK_KEYBOARD(widget);
    GdkRegion       *region;

    kbd->shift ^= 1;
    piboxLogger(LOG_TRACE1, "Shift key: %d\n", kbd->shift);
    gtk_keyboard_paint(widget);
    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_keyboard_increment_page
 * Prototype:  void gtk_keyboard_increment_page( GtkWidget * )
 *
 * Description:
 * Set page identifier to use from keyboard JSON config file.
 * Note that page1 is the default which means "label" and "shiftlabel" are used.
 * Pages > 1 are references as "label<page>" and "shiftlabel<page>".
 *
 * Arguments:
 * GtkWidget *          The keyboard widget.
 *========================================================================
 */
void
gtk_keyboard_increment_page( GtkWidget *widget )
{
    GtkKeyboard     *kbd = GTK_KEYBOARD(widget);
    GdkRegion       *region;

    kbd->activePage = (kbd->activePage+1) % kbd->pages;
    kbd->shift = 0;
    piboxLogger(LOG_TRACE1, "Set keyboard page to: %d\n", kbd->activePage);
    gtk_keyboard_paint(widget);
    region = gdk_drawable_get_clip_region(widget->window);
    gdk_window_invalidate_region(widget->window, region, TRUE);
    gdk_window_process_updates(widget->window, TRUE);
}

/*
 *========================================================================
 * Name:   gtk_keyboard_set_fontsize
 * Prototype:  void gtk_keyboard_set_fontsize( GtkWidget *, guint )
 *
 * Description:
 * Set the size of the font text.  Should be called before the widget is
 * realized.
 * 
 * Arguments:
 * GtkWidget *widget        The keyboard widget.
 *
 * Notes:
 * This should be handled with the gtkrc, but this was the simplest way
 * to implement it for now.
 *========================================================================
 */
void
gtk_keyboard_set_fontsize( GtkWidget *widget, guint size )
{
    GdkRegion       *region;
    GtkKeyboard     *kbd;

    kbd = GTK_KEYBOARD(widget);
    kbd->fontSize = size;
}
